# Relayer

A tool to help run and manage applications not designed to work with the
[Filesystem Hierarchy Standard](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard).

TODO: explain how it works.


# Installation

## Prerequisites

Relayer needs the following software packages to run:
* [Python 3.7 or later](https://www.python.org/)
* [setuptools](https://pypi.org/project/setuptools/)
* [Sorted Containers](https://pypi.org/project/sortedcontainers/)
* [PyXDG](https://pypi.org/project/pyxdg/)
* [FileLock](https://pypi.org/project/filelock/)
* [PSUtil](https://pypi.org/project/psutil/)
* [Tabulate](https://pypi.org/project/tabulate/)
* [Fuse OverlayFS](https://github.com/containers/fuse-overlayfs) is required to use the default fuse-overlayfs driver
* [UnionFS Fuse](https://github.com/rpodgorny/unionfs-fuse) is required to use the legacy unionfs-fuse driver

Additionally, the following are required in order to build Relayer:
* [Babel](https://pypi.org/project/Babel/)
* [Pytest Runner](https://pypi.org/project/pytest-runner/)

And to run the test suite:
* [Pytest](https://pypi.org/project/pytest/)

## Manual installation

Clone the Git repository or download and extract a release tarball and
run the following commands:

```shell
./setup.py build
./setup.py test # Optional, runs the test suite
./setup.py install # May require root privileges
```

## Installation using pip

pip installation is not yet supported.

# Usage

Run ```relayer --help``` to get detailed help.

# Use cases

TODO

# License

Relayer is licensed under the terms of the Mozilla Public License Version 2.0.
