#!/usr/bin/python

# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import fileinput
import shutil
from pathlib import Path

from relayer import RelayerController
from relayer.scopes import SystemScope


def main():
    scope = SystemScope()
    applications_to_rebuild = set()
    for arg in fileinput.input():
        p = Path("/") / arg
        for d in scope.part_search_base_paths():
            if d in p.parents:
                rel_path = p.relative_to(d)
                if len(rel_path.parts) > 0:
                    application = rel_path.parts[0]
                    applications_to_rebuild.add(application)

    if len(applications_to_rebuild) > 0:
        for application in applications_to_rebuild:
            controller = RelayerController(application, scope)
            has_layers = False
            for layerset in controller.layersets():
                controller.modifiers.layerset = layerset.layerset
                controller.application_settings.refresh()
                if len(controller.application_settings.layer_list) != 0:
                    has_layers = True
                    # noinspection PyUnresolvedReferences
                    print(_("Rebuilding {} relayer system cache for {} layer set").format(
                        controller.application_settings.name, layerset.name or _('default')), flush=True)
                    controller.build()
                else:
                    cache = scope.cache_path(controller.application_settings)
                    # noinspection PyUnresolvedReferences
                    print(_("Nuking {} relayer system cache for {} layer set").format(
                        controller.application_settings.name, layerset.name or _('default')), flush=True)
                    try:
                        shutil.rmtree(cache)
                    except FileNotFoundError:
                        # Cache directory does not exist, nothing to delete
                        pass
            if not has_layers:
                cache = scope.cache_path(controller.application_settings)
                if cache.parent.is_dir():
                    # noinspection PyUnresolvedReferences
                    print(_("Nuking {} relayer system cache").format(controller.application_settings.name), flush=True)
                    shutil.rmtree(cache.parent)


if __name__ == "__main__":
    main()
