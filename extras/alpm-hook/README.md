# Relayer ALPM Hook

This post-transaction [ALPM hook](https://www.archlinux.org/pacman/alpm-hooks.5.html)
will take care of rebuilding and cleaning up the Relayer system cache
when a relayer branch is added to, updated on or removed from a
Relayer-managed application.

This will make applications using dynamic branches start faster and will
allow cached branches to be shared between users.

## Installation

As root:
```shell
install -Dm755 alpm-hook.py /usr/lib/relayer/alpm-hook
install -Dm644 relayer.hook /usr/share/libalpm/hooks/relayer.hook
```
