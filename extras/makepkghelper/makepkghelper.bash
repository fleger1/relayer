#!/hint/bash

# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Escape a command line so it can be used as source generator
escape_cmd() {
    while [[ $# -gt 1 ]]; do
        printf "%q " "$1"
        shift
    done
    if  [[ $# == 1 ]]; then
        printf "%q" "$1"
    fi
}
export -f escape_cmd

_print_sub_layer() {
    if in_array "$sublayer" "${global_sublayers}"; then
        echo "sublayer $sublayer already declared" 1>&2
        return 1
    elif [[ "$type" != "dynamic" ]] && [[ "$type" != "static" ]]; then
        echo "type must be defined for each sublayer and its value must either be static or dynamic" 1>&2
        return 1
    elif [[ -z "$source" ]]; then
        echo "source must be defined for each sublayer" 1>&2
        return 1
    fi
    echo "[${sublayer}:Layer]"
    if [[ -n "$name" ]]; then echo "Name = $name"; fi
    if [[ -n "$description" ]]; then echo "Description = $description"; fi
    if [[ -n "$exist" ]]; then echo "IfExists = $exist"; fi
    echo "Type = $type"
    case "$type" in
        "static")
            echo "SourceDirectory = $source"
            ;;
        "dynamic")
            echo "SourceGenerator = $source"
            ;;
    esac
    echo
    if [[ "${#depends[@]}" -gt 0 ]]; then
        echo "[$sublayer:Depends]"
        local d
        for d in "${depends[@]}"; do
            printf "%s\n" "$d"
        done
        echo
    fi
    if [[ "${#conflicts[@]}" -gt 0 ]]; then
        echo "[$sublayer:Conflicts]"
        local c
        for c in "${conflicts[@]}"; do
            printf "%s\n" "$c"
        done
        echo
    fi
    if [[ "${#provides[@]}" -gt 0 ]]; then
        echo "[$sublayer:Provides]"
        local p
        for p in "${provides[@]}"; do
            printf "%s\n" "$p"
        done
        echo
    fi
}
export -f _print_sub_layer

# make_layer -v version [-n name] [-d description] [-u url] [-e exist] [-D dependency] ... [-C conflict] ... [-P provide] ...
#            [[-S sublayer] -t type -s source [-n name] [-d description] [-e exist] [-D depend] ... [-C conflict] ... [-P provide] ...]
# TODO: support l10n (rewrite in Python?)
make_layer() {
    local version
    local url
    local global_name
    local global_description
    local global_exist
    local -a global_depends
    local -a global_conflicts
    local -a global_provides
    local -a global_sublayers=()

    local sublayer
    local type
    local source
    local name
    local description
    local exist
    local -a depends=()
    local -a conflicts=()
    local -a provides=()

    local opt
    local OPTARG
    local OPTIND=1
    while getopts "v:u:n:d:e:D:C:P:t:s:S:" opt; do
        case "$opt" in
            v)
                if [[ -z "$sublayer" ]]; then
                    version="$OPTARG"
                else
                    echo "version not allowed in sublayer declaration" 1>&2
                    return 1
                fi
                ;;
            u)
                if [[ -z "$sublayer" ]]; then
                    url="$OPTARG"
                else
                    echo "url not allowed in sublayer declaration" 1>&2
                    return 1
                fi
                ;;
            n)
                name="$OPTARG"
                ;;
            d)
                description="$OPTARG"
                ;;
            e)
                exist="$OPTARG"
                ;;
            D)
                depends+=("$OPTARG")
                ;;
            C)
                conflicts+=("$OPTARG")
                ;;
            P)
                provides+=("$OPTARG")
                ;;
            t)
                if [[ -n "$sublayer" ]]; then
                    type="$OPTARG"
                else
                    echo "type only allowed in sublayer declaration" 1>&2
                    return 1
                fi
                ;;
            s)
                if [[ -n "$sublayer" ]]; then
                    source="$OPTARG"
                else
                    echo "source only allowed in sublayer declaration" 1>&2
                    return 1
                fi
                ;;
            S)
                if [[ -z "$sublayer" ]]; then
                    # Layer section definition
                    global_name="$name"
                    global_description="$description"
                    global_exist="$exist"
                    global_depends=("${depends[@]}")
                    global_conflicts=("${conflicts[@]}")
                    global_provides=("${provides[@]}")
                else
                    # End of sublayer definition
                    if [[ -z "$OPTARG" ]]; then
                        echo "sublayer must have a non-empty identifier" 1>&2
                        return 1
                    fi
                    if ! _print_sub_layer; then
                        return 1
                    fi
                    global_sublayers+=("$sublayer")
                fi
                sublayer="$OPTARG"
                type=""
                source=""
                name=""
                description=""
                exist=""
                depends=()
                conflicts=()
                provides=()
                ;;
            *)
                {
                    echo "Usage: make_layer -v version [-n name] [-d description] [-u url] [-e exist]"
                    echo "                  [-D dependency] ... [-C conflict] ... [-P provide] ..."
                    echo "                  [[-S sublayer] -t type -s source [-n name] [-d description] [-e exist]"
                    echo "                  [-D depend] ... [-C conflict] ... [-P provide] ...]"
                } 1>&2
                return 1
                ;;
        esac
    done
    shift $((OPTIND - 1))

    # Print last sublayer
    if [[ -z "$sublayer" ]]; then
        echo "at least one sublayer must be declared" 1>&2
        return 1
    else
        if ! _print_sub_layer; then
            return 1
        fi
        global_sublayers+=("$sublayer")
    fi

    # Print Layers section
    echo "[Layers]"
    echo "Version = $version"
    if [[ -n "$url" ]]; then echo "URL = $url"; fi
    if [[ -n "$global_name" ]]; then echo "Name = $global_name"; fi
    if [[ -n "$global_description" ]]; then echo "Description = $global_description"; fi
    if [[ -n "$global_exist" ]]; then echo "IfExists = $global_exist"; fi
    local i=1
    local s
    for s in "${global_sublayers[@]}"; do
        echo "Layer${i} = $s"
        i=$(( i + 1 ))
    done
    echo

    if [[ "${#global_depends[@]}" -gt 0 ]]; then
        echo "[Depends]"
        local d
        for d in "${global_depends[@]}"; do
            printf "%s\n" "$d"
        done
        echo
    fi
    if [[ "${#global_conflicts[@]}" -gt 0 ]]; then
        echo "[Conflicts]"
        local c
        for c in "${global_conflicts[@]}"; do
            printf "%s\n" "$c"
        done
        echo
    fi
    if [[ "${#global_provides[@]}" -gt 0 ]]; then
        echo "[Provides]"
        local p
        for p in "${global_provides[@]}"; do
            printf "%s\n" "$p"
        done
        echo
    fi
}
export -f make_layer

# Mod API
# _mainappname
# _modclass
# _modname
# _modpriority

with_mod() {
    local default_var_name
    for default_var_name in _mainappname _modclass _modpriority _modname; do
        local var_name="${default_var_name}_${1}"
        if [[ -v "${var_name}" ]]; then 
            local "$default_var_name"="${!var_name}"
        fi
    done
    shift
    "$@"
}
export -f with_mod

mod_srcroot() {
    echo "/usr/share/${_mainappname:?}/${_modclass:?}s/${_modname:?}"
}
export -f mod_srcroot

mod_srcdir() {
    echo "$(mod_srcroot)/$1"
}
export -f mod_srcdir

mod_layer() {
    echo "${_modpriority:?}-${_modclass:?}-${_modname:?}.layer"
}
export -f mod_layer

make_mod_layer() {
    local -r SUBLAYER_STATE_MISC=0
    local -r SUBLAYER_STATE_S=1
    local -r SUBLAYER_STATE_NAME=2

    local -a args=(-v "$(get_full_version)" -n "$_modname" -d "$pkgdesc" -P "$_modname" -D "$_mainappname" -u "$url")
    local a
    local sublayer_state=$SUBLAYER_STATE_MISC
    local sublayer
    for a; do
        if [[ $sublayer_state -eq $SUBLAYER_STATE_S ]]; then
            sublayer="$a"
            sublayer_state=$SUBLAYER_STATE_NAME
        elif [[ "$a" == "-S" ]]; then
            sublayer_state=$SUBLAYER_STATE_S
        else
            a="${a//@mod_srcdir@/$(mod_srcdir "$sublayer")}"
            a="${a//@mod_srcdir|esc@/$(escape_cmd "$(mod_srcdir "$sublayer")")}"
            a="${a//@mod_srcroot@/$(mod_srcroot)}"
            a="${a//@mod_srcroot|esc@/$(escape_cmd "$(mod_srcroot)")}"
        fi
        args+=("$a")
        if [[ "$sublayer_state" == "$SUBLAYER_STATE_NAME" ]]; then
            args+=(-P "$_modname/$sublayer")
            sublayer_state=$SUBLAYER_STATE_MISC
        fi
    done
    make_layer "${args[@]}" > "$(mod_layer)"
}
export -f make_mod_layer

with_layerset() {
    local elem="$1"
    local layerset="$2"

    if [[ -z "$layerset" ]]; then
        echo "$elem"
    else
        echo "${elem}.${layerset}"
    fi
}
export -f with_layerset

install_mod_layers() {
    local opt
    local OPTARG
    local OPTIND=1
    local disable=1
    local -a layersets=()
    while getopts "dl:" opt; do
        case "$opt" in
            d)
                disable=0
                ;;
            l)
                layersets+=("$OPTARG")
                ;;
            *)
                echo "Usage: install_mod_layers [-d] [-l layer_set1 ...] layer1 [layer2 ...]" 1>&2
                return 1
        esac
    done
    shift $((OPTIND - 1))
    if [[ ${#layersets[@]} == 0 ]]; then
        layersets+=('')
    fi

    local p
    for p; do
        plain "Installing $p"
        install -Dm644 "$p" -t "${pkgdir:?}/usr/lib/relayer/${_mainappname:?}/"
        if [[ "$disable" == 1 ]]; then
            for layerset in "${layersets[@]}"; do
                plain "Enabling $p for layer set '$layerset'"
                enabled_path="${pkgdir}/usr/lib/relayer/${_mainappname}/$(with_layerset "enabled" "$layerset")/"
                install -dm755 "$enabled_path"
                ln -s "../$(basename "$p")" "$enabled_path/$(basename "$p")"
            done
        fi
    done
}
export -f install_mod_layers

install_mod_resources() {
    local lowercase=1
    local uppercase=1
    local target_dir
    local target=1

    local opt
    local OPTARG
    local OPTIND=1
    local disable=1
    while getopts "ult:" opt; do
        case "$opt" in
            l)
                lowercase=0
                ;;
            u)
                uppercase=0
                ;;
            t)
                target_dir="$OPTARG"
                target=0
                ;;
            *)
                echo "Usage: install_mod_resources [-l|-u] [-t target_dir] sublayer file1 [file2 ...]" 1>&2
                return 1
            esac
    done
    shift $((OPTIND - 1))

    local sublayer="$1"
    shift

    local dest
    dest="$(mod_srcdir "$sublayer")/$target_dir"

    local f
    for f; do
        local d="$f"
        if [[ $lowercase -eq 0 ]]; then
            d="${d,,*}"
        elif [[ $uppercase -eq 0 ]]; then
            d="${d^^*}"
        fi
        if [[ $target -eq 0 ]]; then
            d="$(basename "$d")"
        fi
        if ! [[ -d "$f" ]]; then
            plain "Installing ${f} to ${dest}/${d}"
            install -Dm644 "$f" "${pkgdir}/${dest}/${d}"
        else
            plain "Installing directory ${f} to ${dest}/${d}"
            install -dm755 "${pkgdir}/${dest}/${d}"
        fi
    done
}
export -f install_mod_resources
