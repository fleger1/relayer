Relayer Makepkg Helper
======================

This directory contains library exposing convenience functions to help
packaging Relayer-managed applications with [makepkg](https://wiki.archlinux.org/index.php/Makepkg).

Installation
============
TODO: provide AUR link.

## Dependencies

### Runtime dependencies
* [Pacman](https://www.archlinux.org/pacman/)

### Test dependencies
* [Bats](https://github.com/sstephenson/bats) (ArchLinux package: bash-bats)

## Running the test suite

To run the test suite, make sure you have installed Bats and run:
```shell
./test/makepkghelper.bats
```

## Manual installation
As root:
```shell
install -Dm644 makepkghelper.bash /usr/lib/relayer/makepkghelper.bash
```

## Usage

In your PKGBUILD, add ```'relayer-makepkghelper'``` in your
 makedepends array and source the library in the prepare function:
 ```bash
makedepends=('relayer-makepkghelper')
...
prepare() {
    source "/usr/lib/relayer/makepkghelper.bash"
    ...
}
...
```

## Exposed functions
TODO
