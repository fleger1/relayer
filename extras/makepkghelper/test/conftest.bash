#!/hint/bash

# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Test Helper Funtions
# ====================

# Test equality of values and produces a diff of the two values
# for easier discrepency reporting.
assert_equals() {
    local actual="$1"
    local expected="$2"
    
    if [[ "$expected" != "$actual" ]]; then
        diff -u --label Expected <(echo "$expected") --label Actual <(echo "$actual")
    fi
    
    [[ "$expected" == "$actual" ]]
}

assert_status() {
    if [[ "${status?:}" != "$1" ]]; then
        echo "${output:-}"
    fi
    [[ "$status" == "$1" ]]
}

probe_var() {
    echo "${!1}"
}

# Fake PKGBUILD Environment
# =========================

# makepkghelper expect makepkg util functions
: "${LIBRARY:=/usr/share/makepkg}"
# shellcheck source=/usr/share/makepkg/util/util.sh
source "${LIBRARY}/util/util.sh"
# shellcheck source=/usr/share/makepkg/util/message.sh
source "${LIBRARY}/util/message.sh"
# shellcheck source=/usr/share/makepkg/util/pkgbuild.sh
source "${LIBRARY}/util/pkgbuild.sh"

# mod definition
_mainappname="my app"
_modclass=mod
_modname=mymod
# shellcheck disable=SC2034
_modpriority=50

# 2nd mod
# shellcheck disable=SC2034
_modname_1="$_modname"1
# shellcheck disable=SC2034
_modpriority_1=55

# PKGBUILD variables
# shellcheck disable=SC2034
pkgname="${_mainappname// /}-$_modclass-$_modname"
# shellcheck disable=SC2034
pkgdesc="My Mod"
# shellcheck disable=SC2034
pkgver=1.0
# shellcheck disable=SC2034
pkgrel=1
# shellcheck disable=SC2034
url=https://my.mod

setup() {
    declare -g BUILDDIR="$BATS_TMPDIR/bats_tests_of_${USER}/makepkghelper/$BATS_TEST_NAME"
    rm -rf "$BUILDDIR"
    declare -g srcdir="$BUILDDIR/src"
    declare -g pkgdir="$BUILDDIR/pkg"
    mkdir -p "$srcdir" "$pkgdir"
    pushd "$srcdir" > "/dev/null" || return 1
}

teardown() {
    popd > "/dev/null" || return 1
}
