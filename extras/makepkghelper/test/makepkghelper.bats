#!/usr/bin/env bats

# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

load conftest
load ../makepkghelper

@test "Command escaping" {
    run escape_cmd "/path to/cmd" "arg 1" "arg 2"
    assert_status 0
    assert_equals "$output" '/path\ to/cmd arg\ 1 arg\ 2'
}

@test "Make simple static layer" {
    run make_layer -v 1.0 -S test -t static -s "my source"
    assert_status 0
    assert_equals "$output" '[test:Layer]
Type = static
SourceDirectory = my source

[Layers]
Version = 1.0
Layer1 = test'
}

@test "Make simple dynamic layer" {
    run make_layer -v 1.0 -S test -t dynamic -s "my source"
    assert_status 0
    assert_equals "$output" '[test:Layer]
Type = dynamic
SourceGenerator = my source

[Layers]
Version = 1.0
Layer1 = test'
}

@test "Make complex layer" {
    run make_layer -v 1.0 -n "My Layer" -d "My test layer" -e '/required/file' -u 'http://my.url' -D 'dep1' -C "conflict1" -P 'provide1' -S static -t static -s "my static source" -n "My Static Layer" -d "My test static layer" -e '/required/file/for/static' -D dep2 -C conflict2 -C conflict3 -P provide2 -S dynamic -t dynamic -s "my dynamic source" -n "My Dynamic Layer" -d "My test dynamic layer" -D dep3 -D dep4 -C conflict4 -P provide3
    assert_status 0
    assert_equals "$output" '[static:Layer]
Name = My Static Layer
Description = My test static layer
IfExists = /required/file/for/static
Type = static
SourceDirectory = my static source

[static:Depends]
dep2

[static:Conflicts]
conflict2
conflict3

[static:Provides]
provide2

[dynamic:Layer]
Name = My Dynamic Layer
Description = My test dynamic layer
Type = dynamic
SourceGenerator = my dynamic source

[dynamic:Depends]
dep3
dep4

[dynamic:Conflicts]
conflict4

[dynamic:Provides]
provide3

[Layers]
Version = 1.0
URL = http://my.url
Name = My Layer
Description = My test layer
IfExists = /required/file
Layer1 = static
Layer2 = dynamic

[Depends]
dep1

[Conflicts]
conflict1

[Provides]
provide1'
}

@test "Local mod override" {
    run with_mod 1 probe_var _mainappname
    assert_status 0
    assert_equals "$output" "my app"
    run with_mod 1 probe_var _modclass
    assert_status 0
    assert_equals "$output" mod
    run with_mod 1 probe_var _modpriority
    assert_status 0
    assert_equals "$output" 55
    run with_mod 1 probe_var _modname
    assert_status 0
    assert_equals "$output" mymod1
}

@test "Mod source root" {
    run mod_srcroot
    assert_status 0
    assert_equals "$output" "/usr/share/my app/mods/mymod"
}

@test "Mod source directory" {
    run mod_srcdir "base"
    assert_status 0
    assert_equals "$output" "/usr/share/my app/mods/mymod/base"
}

@test "Mod layer name" {
    run mod_layer
    assert_status 0
    # shellcheck disable=SC2154
    assert_equals "$output" "$_modpriority-$_modclass-$_modname.layer"
}

@test "Make mod layer" {
    run make_mod_layer -S static -t static -s @mod_srcdir@ \
                       -S dynamic -t dynamic -s "gen_mod @mod_srcdir|esc@" -D "$_modname/static" \
                       -S dynamic2 -t dynamic -s "gen_mod @mod_srcroot|esc@" -D "$_modname/static"
    assert_status 0
    [[ -f "$_modpriority-$_modclass-$_modname.layer" ]]
    assert_equals "$(< "$_modpriority-$_modclass-$_modname.layer")" '[static:Layer]
Type = static
SourceDirectory = /usr/share/my app/mods/mymod/static

[static:Provides]
mymod/static

[dynamic:Layer]
Type = dynamic
SourceGenerator = gen_mod /usr/share/my\ app/mods/mymod/dynamic

[dynamic:Depends]
mymod/static

[dynamic:Provides]
mymod/dynamic

[dynamic2:Layer]
Type = dynamic
SourceGenerator = gen_mod /usr/share/my\ app/mods/mymod

[dynamic2:Depends]
mymod/static

[dynamic2:Provides]
mymod/dynamic2

[Layers]
Version = 1.0-1
URL = https://my.mod
Name = mymod
Description = My Mod
Layer1 = static
Layer2 = dynamic
Layer3 = dynamic2

[Depends]
my app

[Provides]
mymod'
}

@test "Install mod layers" {
    > "1-mod-enabled.layer"
    > "2-mod-disabled.layer"
    run install_mod_layers "1-mod-enabled.layer"
    assert_status 0
    run install_mod_layers -d "2-mod-disabled.layer"
    assert_status 0
    [[ -f "$pkgdir/usr/lib/relayer/my app/1-mod-enabled.layer" ]]
    [[ -f "$pkgdir/usr/lib/relayer/my app/2-mod-disabled.layer" ]]
    [[ -h "$pkgdir/usr/lib/relayer/my app/enabled/1-mod-enabled.layer" ]]
    [[ "$(readlink "$pkgdir/usr/lib/relayer/my app/enabled/1-mod-enabled.layer")" == "../1-mod-enabled.layer" ]]
    ! [[ -e "$pkgdir/usr/lib/relayer/my app/enabled/2-mod-disabled.layer" ]]
}

@test "Install mod layers with layerset" {
    > "1-mod-enabled.layer"
    > "2-mod-disabled.layer"
    run install_mod_layers -l 'alt' -l '' "1-mod-enabled.layer"
    assert_status 0
    run install_mod_layers -l 'alt' "2-mod-disabled.layer"
    assert_status 0
    [[ -f "$pkgdir/usr/lib/relayer/my app/1-mod-enabled.layer" ]]
    [[ -f "$pkgdir/usr/lib/relayer/my app/2-mod-disabled.layer" ]]
    [[ -h "$pkgdir/usr/lib/relayer/my app/enabled/1-mod-enabled.layer" ]]
    [[ -h "$pkgdir/usr/lib/relayer/my app/enabled.alt/1-mod-enabled.layer" ]]
    [[ "$(readlink "$pkgdir/usr/lib/relayer/my app/enabled/1-mod-enabled.layer")" == "../1-mod-enabled.layer" ]]
    [[ "$(readlink "$pkgdir/usr/lib/relayer/my app/enabled.alt/1-mod-enabled.layer")" == "../1-mod-enabled.layer" ]]
    ! [[ -e "$pkgdir/usr/lib/relayer/my app/enabled/2-mod-disabled.layer" ]]
    [[ -h "$pkgdir/usr/lib/relayer/my app/enabled.alt/2-mod-disabled.layer" ]]
    [[ "$(readlink "$pkgdir/usr/lib/relayer/my app/enabled.alt/2-mod-disabled.layer")" == "../2-mod-disabled.layer" ]]
}

@test "Install mod resources" {
    mkdir x
    > RES1
    > res2
    > x/Res3
    > res4
    > x/res5
    
    run install_mod_resources -l static RES1
    assert_status 0
    run install_mod_resources -u static res2
    assert_status 0
    run install_mod_resources static x/Res3
    assert_status 0
    run install_mod_resources -t override static res4 x/res5
    assert_status 0
    
    [[ -f "$pkgdir/usr/share/my app/mods/mymod/static/res1" ]]
    [[ -f "$pkgdir/usr/share/my app/mods/mymod/static/RES2" ]]
    [[ -f "$pkgdir/usr/share/my app/mods/mymod/static/x/Res3" ]]
    [[ -f "$pkgdir/usr/share/my app/mods/mymod/static/override/res4" ]]
    [[ -f "$pkgdir/usr/share/my app/mods/mymod/static/override/res5" ]]
}
