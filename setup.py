#!/usr/bin/env python

# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import versioneer

from pathlib import Path
from setuptools import setup, find_packages
from babel.messages import frontend as babel


def mo_files():
    # *.mo files may not have been generated yet,
    # so find *.po files instead and change the extension
    for p in Path("locale/").glob("*/*/*.po"):
        yield str(Path("share") / p.parent), [str(p.parent / (p.stem + ".mo"))]


# Make sure we build the *.po files during the build process
cmd_class = versioneer.get_cmdclass()


class relayer_build_py(cmd_class['build_py']):

    def run(self) -> None:
        super().run()
        self.run_command('compile_catalog')


cmd_class['build_py'] = relayer_build_py


class ExtractMessages(babel.extract_messages):

    def finalize_options(self):
        # Generate translations for the argparse module too as they are not included in Python
        import argparse
        # noinspection PyAttributeOutsideInit
        self.input_paths = ",".join((self.input_paths, argparse.__file__))
        return super().finalize_options()


setup(
    name="relayer",
    version=versioneer.get_version(),
    packages=find_packages("src"),
    package_dir={'': 'src'},
    data_files=list(mo_files()),
    cmdclass={
        **{'compile_catalog': babel.compile_catalog,
           'extract_messages': ExtractMessages,
           'init_catalog': babel.init_catalog,
           'update_catalog': babel.update_catalog
           },
        **cmd_class
    },
    install_requires=['sortedcontainers', 'pyxdg', 'filelock', 'psutil', 'tabulate'],
    license="MPL 2.0",
    author="Florian Léger",
    author_email="florian.leger6+relayer@gmail.com",
    description="Run and manage applications not designed to work with the FHS",
    keywords="unionfs overlayfs mod game utility",
    entry_points={
        'relayer.mount_drivers': [
            "unionfs-fuse = relayer.mount_drivers:UnionFSFuseMountDriver",
            "fuse-overlayfs = relayer.mount_drivers:FuseOverlayFSMountDriver",
        ],
        'relayer.branch_generators': [
            "static = relayer.generators:static_branch_generator",
            "dynamic = relayer.generators:dynamic_branch_generator",
        ],
        'relayer.scopes': [
            "user = relayer.scopes:UserScope",
            "system = relayer.scopes:SystemScope",
        ],
        'console_scripts': [
            "relayer = relayer.cli:main"
        ]
    },
    url='https://gitlab.com/fleger1/relayer',
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3.7",
        "Topic :: System :: Software Distribution",
    ],
    setup_requires=['babel'],
    tests_require=['pytest', 'pytest-runner'],
)
