# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import itertools
import json
import logging
import os
import stat
import subprocess
import sys
from os import getuid, getgid
from pathlib import Path
from typing import List, Optional, Any

from . import MountDriver, ApplicationSettings, MountDriverException

_logger = None


def logger():
    """Returns the logger instance used in this module."""
    global _logger
    _logger = _logger or logging.getLogger(__name__)
    return _logger


class UnionFSFuseMountDriver(MountDriver):
    @classmethod
    def priority(cls) -> int:
        return 10

    @classmethod
    def describe(cls) -> str:
        return _("UnionFS Fuse driver")

    def __init__(self) -> None:
        super().__init__()
        self.unionfs_path: str = "unionfs"
        self.fusermount_path: str = "fusermount3"

    def cookie_path(self, application_settings: ApplicationSettings) -> Path:
        return application_settings.scope.runtime_path(application_settings.application) / \
               f"{application_settings.layerset.with_layerset('unionfs')}.cookie"

    def write_cookie(self, application_settings: ApplicationSettings, data: Any) -> None:
        cookie_path = self.cookie_path(application_settings)
        application_settings.scope.mkdir_and_return(cookie_path.parent)
        with cookie_path.open('w') as cookie:
            json.dump(data, cookie)

    def read_cookie(self, application_settings: ApplicationSettings) -> Any:
        data = None
        cookie_path = self.cookie_path(application_settings)
        if cookie_path.is_file():
            try:
                with cookie_path.open('r') as cookie:
                    data = json.load(cookie)
            except json.JSONDecodeError:
                pass
        return data

    def erase_cookie(self, application_settings: ApplicationSettings) -> None:
        self.cookie_path(application_settings).unlink(missing_ok=True)

    def mount(self,
              application_settings: ApplicationSettings,
              upper_dir: Optional[Path] = None,
              up_to: Optional[str] = None) -> Path:
        lower_branches: List[Path] = self.make_lower_branches(application_settings.layer_list,
                                                              up_to, application_settings.modifiers).branches
        mount_point: Path = self.make_final_mount_point(application_settings)
        upper_dir: Path = upper_dir if upper_dir is not None else self.make_rw_user_dir(application_settings)
        logger().debug(f"Mounting RW branch {upper_dir} and RO branches {lower_branches} into {mount_point}")
        cmd = [self.unionfs_path, "-o", f"uid={getuid()}", "-o", f"gid={getgid()}"]
        if application_settings.use_copy_on_write:
            cmd.extend(("-o", "cow"))
        cmd.append(":".join(s for s in itertools.chain((f"{upper_dir}=RW",),
                                                       (f"{b}=RO" for b in lower_branches[::-1]))))
        cmd.append(mount_point)

        self.write_cookie(application_settings, {"upper_dir": f"{upper_dir.resolve()}"})

        logger().debug(f"Mount command: {cmd}")
        subprocess.run(
            cmd,
            check=True,
            stdout=sys.stdout,
            stderr=sys.stderr
        )
        return mount_point

    def umount(self, application_settings: ApplicationSettings, lazy: bool = False) -> None:
        mount_point = self.make_final_mount_point(application_settings)
        if mount_point.is_mount():
            upper_dir = None
            cookie = self.read_cookie(application_settings)
            if isinstance(cookie, dict):
                upper_dir = cookie.get("upper_dir", None)
            logger().debug(f"Unmounting {mount_point}")
            cmd = [self.fusermount_path, "-u"]
            if lazy:
                cmd.append("-z")
            cmd.append(mount_point)
            subprocess.run(cmd,
                           check=True, stdout=sys.stdout, stderr=sys.stderr)
            self.erase_cookie(application_settings)
            if upper_dir is not None:
                self.fix_metadata_permissions(Path(upper_dir))

    @staticmethod
    def chmod_plus(path: Path, mode_plus: int) -> None:
        """
        Equivalent to using chmod +something
        :param path: file path
        :param mode_plus: permission bits to add
        """
        mode = path.stat().st_mode & ~0o770000
        path.chmod(mode | mode_plus)

    def fix_metadata_permissions(self, upper_dir: Path) -> None:
        """
        Fix unionfs metadata permissions to them readable by other users.

        This is a workaround to fix the following use case:
            - Dynamic layers are generated as root in the system scope.
              During layer generation files from lower layers can be deleted,
              thus triggering the creation of the .unionfs metadata directory
              containing the writeout information. Unfortunately this directory
              is not readable by other users.
            - When a non-root user mounts the application, it does not have
              sufficient permissions to access the metadata from the system cached
              dynamic layers, so the deleted files are still appearing because the
              writeout information can't be read.

        :param upper_dir: rw branch path containing the metadata directory
        """
        metadata_root = upper_dir / ".unionfs"
        if not metadata_root.is_dir():
            return
        self.chmod_plus(upper_dir, stat.S_IRGRP | stat.S_IROTH | stat.S_IXGRP | stat.S_IXOTH)
        for root, dirs, files in os.walk(upper_dir):
            for d in dirs:
                d = Path(root) / d
                self.chmod_plus(d, stat.S_IRGRP | stat.S_IROTH | stat.S_IXGRP | stat.S_IXOTH)
            for f in files:
                f = Path(root) / f
                self.chmod_plus(f, stat.S_IRGRP | stat.S_IROTH)


class FuseOverlayFSMountDriverException(MountDriverException):
    pass


class FuseOverlayFSMountDriver(MountDriver):
    @classmethod
    def priority(cls) -> int:
        return 15

    @classmethod
    def describe(cls) -> str:
        return _("Fuse OverlayFS driver")

    def __init__(self) -> None:
        super().__init__()
        self.fuse_overlayfs_path: str = "fuse-overlayfs"
        self.fusermount_path: str = "fusermount3"

    @staticmethod
    def make_work_user_dir(application_settings: ApplicationSettings) -> Path:
        """Create and return OverlayFS working directory"""
        return application_settings.scope.mkdir_and_return(
            application_settings.scope.data_path(
                application_settings.application) / application_settings.layerset.with_layerset("fuse-overlayfs-work"))

    def mount(self,
              application_settings: ApplicationSettings,
              upper_dir: Optional[Path] = None,
              up_to: Optional[str] = None) -> Path:

        if not application_settings.use_copy_on_write:
            raise FuseOverlayFSMountDriverException(_("Copy On Write can't be disabled when using Fuse OverlayFS"))

        lower_branches: List[Path] = self.make_lower_branches(application_settings.layer_list,
                                                              up_to, application_settings.modifiers).branches
        mount_point: Path = self.make_final_mount_point(application_settings)
        upper_dir: Path = upper_dir if upper_dir is not None else self.make_rw_user_dir(application_settings)
        work_dir = self.make_work_user_dir(application_settings)
        logger().debug(f"Mounting RW branch {upper_dir} and RO branches {lower_branches} into {mount_point}")
        cmd = [self.fuse_overlayfs_path, "-o", f"squash_to_uid={getuid()}", "-o", f"squash_to_gid={getgid()}"]
        if lower_branches:
            cmd.append('-o')
            cmd.append(f"lowerdir={':'.join(str(p) for p in lower_branches[::-1])}")
        cmd.extend(('-o', f"upperdir={upper_dir.resolve()}"))
        cmd.extend(('-o', f"workdir={work_dir.resolve()}"))
        cmd.append(mount_point)

        logger().debug(f"Mount command: {cmd}")
        subprocess.run(
            cmd,
            check=True,
            stdout=sys.stdout,
            stderr=sys.stderr
        )
        return mount_point

    def umount(self, application_settings: ApplicationSettings, lazy: bool = False) -> None:
        mount_point = self.make_final_mount_point(application_settings)
        if mount_point.is_mount():
            logger().debug(f"Unmounting {mount_point}")
            cmd = [self.fusermount_path, "-u"]
            if lazy:
                cmd.append("-z")
            cmd.append(mount_point)
            subprocess.run(cmd,
                           check=True, stdout=sys.stdout, stderr=sys.stderr)
