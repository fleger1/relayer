# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import argparse
import functools
import logging
import operator
from inspect import signature
from typing import Callable, Optional

from tabulate import tabulate

from . import all_scopes, all_mount_drivers, RelayerController, SubLayerCheck, LayerCheck, \
    SwitchReturnStatus, __version__

# Force argparse to use built-in relayer gettext catalog
# noinspection PyProtectedMember
del argparse._
del argparse.ngettext


def action(f):
    @functools.wraps(f)
    def wrapper(namespace: argparse.Namespace):
        scope = all_scopes[namespace.scope]()
        mount_driver = all_mount_drivers[namespace.mount_driver]()
        s = signature(f)
        kwargs = {}
        if "namespace" in s.parameters:
            kwargs["namespace"] = namespace
        controller = RelayerController(namespace.application[0], scope, mount_driver)
        if "enable" in namespace and namespace.enable is not None:
            for layer in namespace.enable:
                controller.modifiers.enable.add(layer)
        if "disable" in namespace and namespace.disable is not None:
            for layer in namespace.disable:
                controller.modifiers.disable.add(layer)
        if "layerset" in namespace:
            controller.modifiers.layerset = namespace.layerset
        return f(controller, **kwargs)

    return wrapper


@action
def mount(controller: RelayerController) -> None:
    path = controller.mount()
    print(_("Application mounted in {}").format(path))


@action
def build(controller: RelayerController) -> None:
    controller.build()


@action
def umount(controller: RelayerController, namespace: argparse.Namespace) -> None:
    path = controller.umount(lazy=namespace.lazy)
    print(_("Unmounted application from {}").format(path))


@action
def run(controller: RelayerController, namespace: argparse.Namespace) -> int:
    return controller.run(namespace.command, namespace.change_directory, namespace.shell,
                          lazy=namespace.lazy)


@action
def mount_path(controller: RelayerController) -> None:
    print(controller.mount_path())


@action
def list_layers(controller: RelayerController) -> None:
    for check in controller.check_layers():
        print(
            f" {'✓' if check.enabled else '!' if (check.symlink_enabled or check.force_enabled) and not check.enabled else '✗' if (not check.symlink_enabled and check.layer.symlink) or check.force_disabled else ' '} "
            f"[{check.layer.layer_name}]{f' {check.layer.name}' if check.layer.name != check.layer.layer_name else ''}")
        for i, s_check in enumerate(check.sub_layer_checker):
            print(
                f"   {'├' if i < len(check.layer.sub_layers) - 1 else '└'}─ "
                f"{'✓' if s_check.enabled else '!' if (check.symlink_enabled or check.force_enabled) and not s_check.enabled else '✗' if not (not check.symlink_enabled and check.layer.symlink) or check.force_disabled else ' '} "
                f"[{s_check.sub_layer.sublayer_name}]{f' {s_check.sub_layer.name}' if s_check.sub_layer.name != s_check.sub_layer.sublayer_name else ''}"
            )


def format_collection(c) -> str:
    return ", ".join(c) if len(c) > 0 else '--'


@action
def info(controller: RelayerController, namespace: argparse.Namespace) -> None:
    sub_layer_names = set(namespace.layers)
    for check in controller.check_layers():
        layer_info_displayed = False
        display_all_sub_layers = False
        if not sub_layer_names.isdisjoint(check.layer.provides):
            display_layer_info(check)
            layer_info_displayed = True
            display_all_sub_layers = True
        for s_check in check.sub_layer_checker:
            if display_all_sub_layers or not sub_layer_names.isdisjoint(s_check.sub_layer.provides):
                if not layer_info_displayed:
                    display_layer_info(check)
                    layer_info_displayed = True
                display_sub_layer_info(s_check)


@action
def enable(controller: RelayerController, namespace: argparse.Namespace) -> Optional[int]:
    r = controller.enable(namespace.layer)
    if r == SwitchReturnStatus.OK:
        print(_("{} enabled").format(namespace.layer))
        return
    if r == SwitchReturnStatus.NO_OP:
        print(_("{} already enabled").format(namespace.layer))
        return
    if r == SwitchReturnStatus.NO_MATCHING_LAYER:
        print(_("No layer named {} found").format(namespace.layer))
        return 1


@action
def disable(controller: RelayerController, namespace: argparse.Namespace) -> Optional[int]:
    r = controller.disable(namespace.layer)
    if r == SwitchReturnStatus.OK:
        print(_("{} disabled").format(namespace.layer))
        return
    if r == SwitchReturnStatus.NO_OP:
        print(_("{} already disabled").format(namespace.layer))
        return
    if r == SwitchReturnStatus.NO_MATCHING_LAYER:
        print(_("No layer named {} found").format(namespace.layer))
        return 1


@action
def layer_sets(controller: RelayerController) -> Optional[int]:
    layersets = controller.layersets()
    for layerset in sorted(layersets, key=operator.attrgetter('layerset')):
        print(f"{'✓' if layerset == controller.application_settings.layerset else ' '} "
              f"[{layerset.layerset or ''}]{f' {layerset.name}' if layerset.name != layerset.layerset else ''}"
              f"{f' --> [{layerset.parent.layerset}]' if layerset.parent else ''}")
    return


def display_sub_layer_info(s_check: SubLayerCheck) -> None:
    print(tabulate([
        [_("SubLayer Id:"), s_check.sub_layer.layer_name],
        [_("Name:"), s_check.sub_layer.name],
        [_("Description:"), s_check.sub_layer.description if s_check.sub_layer.description != '' else '--'],
        [_("Provides:"),
         format_collection([s for s in s_check.sub_layer.provides if s != s_check.sub_layer.layer_name])],
        [_("Depends on:"), format_collection(s_check.sub_layer.depends)],
        [_("Conflicts with:"), format_collection(s_check.sub_layer.conflicts)],
        [_("Requires file:"), s_check.sub_layer.if_exists if s_check.sub_layer.if_exists else '--'],
        [_("Status:"), _("Enabled") if s_check.enabled else _("Disabled")],
        [_("Missing dependencies:"), format_collection(s_check.missing_depends)],
        [_("Conflicting layers:"), format_collection(s_check.conflicting_layers)],
        [_("Required file exists:"), _("Yes") if s_check.exists else _("No")],
    ]))


def display_layer_info(check: LayerCheck) -> None:
    print(tabulate([
        [_("Id:"), check.layer.layer_name],
        [_("Name:"), check.layer.name],
        [_("Description:"), check.layer.description if check.layer.description != '' else '--'],
        [_("Version:"), check.layer.version],
        [_("Location:"), check.layer.layer_file],
        [_("Provides:"), format_collection([s for s in check.layer.provides if s != check.layer.layer_name])],
        [_("Depends on:"), format_collection(check.layer.depends)],
        [_("Conflicts with:"), format_collection(check.layer.conflicts)],
        [_("Requires file:"), check.layer.if_exists if check.layer.if_exists else '--'],
        [_("Status:"), _("Enabled") if check.enabled else _("Disabled")],
        [_("Missing dependencies:"), format_collection(check.missing_depends)],
        [_("Conflicting layers:"), format_collection(check.conflicting_layers)],
        [_("Required file exists:"), _("Yes") if check.exists else _("No")],
        [_("Enabled by symlink:"),
         check.layer.symlink if check.layer.symlink and check.symlink_enabled else '--'],
        [_("Disabled by symlink:"),
         check.layer.symlink if check.layer.symlink and not check.symlink_enabled else '--'],
    ]))


def make_action_subparser(subparsers,
                          func: Callable[[argparse.Namespace], Optional[int]],
                          name: str, help: str):
    action_parser = subparsers.add_parser(name, help=help)
    action_parser.add_argument("application", metavar=_("APPLICATION"), help=_("Application"), nargs=1)
    action_parser.set_defaults(func=func)
    action_parser.add_argument("--layer-set", '-l', metavar=_("LAYER_SET"), type=str, dest="layerset",
                               help=_("Layer Set"), required=False, default=None)
    return action_parser


def add_modifier_params(subparser):
    subparser.add_argument("--enable", metavar=_("LAYER"), action="append", type=str, dest="enable",
                           help=_("Explicitely enable a layer for the duration of this invocation"))
    subparser.add_argument("--disable", metavar=_("LAYER"), action="append", type=str, dest="disable",
                           help=_("Explicitely disable a layer for the duration of this invocation"))
    return subparser


def add_unmount_params(subparser):
    subparser.add_argument("--lazy", help=_("Lazy unmount"), required=False, action="store_true")
    return subparser


def build_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=_("Manage and run applications not designed to work with the FHS"),
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--version", "-v", action="version", version=f"%(prog)s {__version__}"
                        , help=_("show program's version number and exit"))
    parser.add_argument("--scope", "-s", metavar=_("SCOPE"), type=str, choices=all_scopes.keys(),
                        default=next(iter(all_scopes.keys())),
                        dest="scope", help=_("path scope"), required=False)
    parser.add_argument("--driver", "-d", metavar=_("DRIVER"), type=str, choices=all_mount_drivers.keys(),
                        default=next(iter(all_mount_drivers.keys())),
                        dest="mount_driver", help=_("mount driver"), required=False)
    parser.add_argument("--log", type=str, choices=logging._levelToName.values(), default="WARNING", dest="log_level",
                        help=_("log level"))
    action_subparsers = parser.add_subparsers(title=_("Actions"), description=_("Action to perform"),
                                              help=_("action name"))
    make_action_subparser(action_subparsers, mount_path, "mount-path", _("show application mount path"))
    action_subparser = make_action_subparser(action_subparsers, mount, "mount", _("manually mount application"))
    add_modifier_params(action_subparser)
    action_subparser = make_action_subparser(action_subparsers, umount, "umount", _("manually unmount application"))
    add_unmount_params(action_subparser)
    action_subparser = make_action_subparser(action_subparsers, build, "build",
                                             _("force building cached dynamic branches"))
    add_modifier_params(action_subparser)
    action_subparser = make_action_subparser(action_subparsers, list_layers, "list", _("list layers"))
    add_modifier_params(action_subparser)
    action_run = make_action_subparser(action_subparsers, run, "run", help=_("mount application, run command, unmount"))
    add_modifier_params(action_run)
    add_unmount_params(action_run)
    action_run.add_argument("command", help=_("command to execute"), nargs=argparse.ONE_OR_MORE)
    action_run.add_argument("--shell", help=_("run command in a shell"), required=False, action="store_true")
    action_run.add_argument("--change-directory",
                            help=_("change working directory to mount directory before running the command"),
                            required=False, action="store_true", dest="change_directory")
    action_subparser = make_action_subparser(action_subparsers, info, "info",
                                             help=_("display information on layers or sub-layers"))
    add_modifier_params(action_subparser)
    action_subparser.add_argument("layers", metavar=_("LAYER"), help=_("layer id"), nargs=argparse.ONE_OR_MORE)
    action_subparser = make_action_subparser(action_subparsers, enable, "enable",
                                             help=_("enable layer"))
    action_subparser.add_argument("layer", metavar=_("LAYER"), help=_("layer id"))
    action_subparser = make_action_subparser(action_subparsers, disable, "disable",
                                             help=_("disable layer"))
    action_subparser.add_argument("layer", metavar=_("LAYER"), help=_("layer id"))
    make_action_subparser(action_subparsers, layer_sets, "layer-sets", help=_("list available layer sets"))
    return parser


def cli(*args) -> Optional[int]:
    parser = build_parser()
    namespace = parser.parse_args(args)
    logging.basicConfig(level=namespace.log_level)
    if not hasattr(namespace, "func"):
        print(_("No action specified!"))
        parser.print_usage()
        return 2
    return namespace.func(namespace)


def main() -> None:
    import sys
    sys.exit(cli(*sys.argv[1:]))


if __name__ == "__main__":
    main()
