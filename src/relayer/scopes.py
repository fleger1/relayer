# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from pathlib import Path
from typing import Iterable

from xdg import BaseDirectory

from . import Scope, APPLICATION_NAME, ApplicationSettings


class SystemScope(Scope):
    """
    Path manager meant to be used for system-wide mono-user services
    """

    @classmethod
    def priority(cls) -> int:
        return 5

    @classmethod
    def describe(cls) -> str:
        return _("Limit to system paths")

    def cache_search_paths(self, application_settings: ApplicationSettings) -> Iterable[Path]:
        """Root cache search paths for applications (e. g. where expanded dynamic parts are looked up)"""
        application = application_settings.application
        layers = application_settings.layerset.with_layerset('layers')
        yield Path("/var") / "cache" / APPLICATION_NAME / application / layers

    def application_settings_paths(self, application: str) -> Iterable[Path]:
        """Paths to search for parts. Lowest priority path first."""
        yield Path("/usr") / "lib" / APPLICATION_NAME / (application + ".conf")
        yield Path("/etc") / APPLICATION_NAME / (application + ".conf")

    def layer_search_paths(self, application: str) -> Iterable[Path]:
        """Paths to search for parts. Highest priority path first."""
        return (p / f"{application}" for p in self.part_search_base_paths())

    def layer_path(self, application: str) -> Path:
        return Path("/etc") / APPLICATION_NAME / (application + ".conf")

    @staticmethod
    def part_search_base_paths() -> Iterable[Path]:
        yield Path("/etc") / APPLICATION_NAME
        yield Path("/usr") / "lib" / APPLICATION_NAME

    def cache_path(self, application_settings: ApplicationSettings) -> Path:
        """Root cache path for applications (e. g. where dynamic parts are built)"""
        return next(iter(self.cache_search_paths(application_settings)))

    def data_path(self, application: str) -> Path:
        """Path used to save persistent user data (e. g. R/W branch)"""
        return Path("/var") / "lib" / APPLICATION_NAME / application

    def runtime_path(self, application: str) -> Path:
        """Path used to store temporary runtime data (e. g. mountpoints)"""
        return self.mkdir_and_return(Path("/run") / APPLICATION_NAME / application)


class UserScope(SystemScope):
    """
    Path manager meant to be used for multi-user applications
    """

    @classmethod
    def priority(cls) -> int:
        return 10

    @classmethod
    def describe(cls) -> str:
        return _("Use both system and per-user paths")

    def cache_search_paths(self, application_settings: ApplicationSettings) -> Iterable[Path]:
        yield Path(BaseDirectory.save_cache_path(APPLICATION_NAME, application_settings.application,
                                                 application_settings.layerset.with_layerset('layers')))
        yield from super().cache_search_paths(application_settings)

    def data_path(self, application: str) -> Path:
        """Path where to save user data (e. g. R/W branch)"""
        return Path(BaseDirectory.save_data_path(APPLICATION_NAME, application))

    def runtime_path(self, application: str) -> Path:
        """Path where to store temporary runtime data (e. g. mountpoints)"""
        return self.mkdir_and_return(Path(BaseDirectory.get_runtime_dir(True)) / APPLICATION_NAME / application)

    def layer_search_paths(self, application: str) -> Iterable[Path]:
        """Paths to search for parts."""
        yield from (Path(p) for p in BaseDirectory.load_config_paths(APPLICATION_NAME, application))
        yield from super().layer_search_paths(application)

    def layer_path(self, application: str) -> Path:
        return Path(BaseDirectory.save_config_path(APPLICATION_NAME, application))

    def application_settings_paths(self, application: str) -> Iterable[Path]:
        yield from super().application_settings_paths(application)
        yield from (Path(p) / (application + ".conf")
                    for p in reversed(list(BaseDirectory.load_config_paths(APPLICATION_NAME))))
