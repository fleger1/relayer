# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Postpone annotation evaluation
from __future__ import annotations

import itertools
import sys

import json
import logging
import shutil
import subprocess
from pathlib import Path
from typing import Iterator, List, Optional

from . import UnavailableMountResourceException, \
    BranchGenerationResult, BranchGenerator, SubLayer, get_param_value, MountEnvironment

_logger = None


def logger():
    """Returns the logger instance used in this module."""
    global _logger
    _logger = _logger or logging.getLogger(__name__)
    return _logger


class StaticBranchGenerator(BranchGenerator):

    def generate_branch(self, mount_environment: MountEnvironment, sub_layer: SubLayer) -> BranchGenerationResult:
        assert sub_layer.type == "static"
        source_directory = Path(get_param_value("SourceDirectory", sub_layer.layer_section))
        if not source_directory.is_dir():
            raise UnavailableMountResourceException(f"{source_directory} is not an existing directory")
        return BranchGenerationResult(source_directory)


class DynamicBranchGenerator(BranchGenerator):
    class CachedPaths:
        def __init__(self, sub_layer: SubLayer, cache_root: Path) -> None:
            self.sub_layer = sub_layer
            self.cache_root = cache_root

        @property
        def source_directory(self) -> Path:
            return self.cache_root / f"{self.sub_layer.layer_name}.dynamic"

        @property
        def trail_file(self) -> Path:
            return self.cache_root / f"{self.sub_layer.layer_name}.dynamic.json"

    @staticmethod
    def __to_trail(previous_layers: Iterator[SubLayer], current_sub_layer) -> List[List[str]]:
        return [[p.layer_name, p.layer.version] for p in itertools.chain(previous_layers, (current_sub_layer,))]

    def generate_branch(self, mount_environment: MountEnvironment, sub_layer: SubLayer) -> BranchGenerationResult:
        assert sub_layer.type == "dynamic"
        current_trail = self.__to_trail(mount_environment.previous_sub_layers, sub_layer)
        logger().info(f"Generating layer {sub_layer}, computed trail is {current_trail}")
        source_directory = self.__find_valid_cache(current_trail, sub_layer)
        if source_directory is None:
            source_directory = self.__rebuild_cached_branch(mount_environment, current_trail, sub_layer)
        return source_directory

    def __rebuild_cached_branch(self,
                                mount_environment: MountEnvironment,
                                trail: List[List[str]],
                                sub_layer: SubLayer) -> BranchGenerationResult:
        cached_paths = self.CachedPaths(sub_layer,
                                        sub_layer.layer.settings.scope.cache_path(sub_layer.layer.settings))
        logger().info(f"Building dynamic branch for {sub_layer} in {cached_paths.source_directory}")
        if cached_paths.trail_file.is_file():
            cached_paths.trail_file.unlink()
        if cached_paths.source_directory.is_dir():
            # noinspection PyTypeChecker
            shutil.rmtree(cached_paths.source_directory)
        sub_layer.layer.settings.scope.mkdir_and_return(cached_paths.source_directory)
        mount_point = mount_environment.mount_driver.mount(sub_layer.layer.settings,
                                                           upper_dir=cached_paths.source_directory,
                                                           up_to=sub_layer.layer_name)
        try:
            source_generator = get_param_value("SourceGenerator", sub_layer.layer_section)
            logger().debug(f"Executing {sub_layer} dynamic generator {source_generator} in {mount_point}")
            import shlex
            subprocess.run([s.replace("@mount_point@", str(mount_point)) for s in shlex.split(source_generator)],
                           check=True,
                           stdout=sys.stdout, stderr=sys.stderr,
                           env=sub_layer.layer.settings.make_child_environment(mount_point))
        except FileNotFoundError as e:
            raise UnavailableMountResourceException(str(e))
        finally:
            mount_environment.mount_driver.umount(sub_layer.layer.settings)
        logger().debug(f"Writing trail {trail} in {cached_paths.trail_file}")
        cached_paths.trail_file.touch()
        with cached_paths.trail_file.open("w") as fp:
            json.dump(trail, fp)
        return BranchGenerationResult(cached_paths.source_directory,
                                      {cached_paths.source_directory, cached_paths.trail_file})

    def __find_valid_cache(self, trail: List[List[str]], sub_layer: SubLayer) -> Optional[BranchGenerationResult]:
        for cache_path in sub_layer.layer.settings.scope.cache_search_paths(sub_layer.layer.settings):
            cached_paths = self.CachedPaths(sub_layer, cache_path)
            persisted_trail = None
            if cached_paths.source_directory.is_dir() and cached_paths.trail_file.is_file():
                try:
                    with cached_paths.trail_file.open() as fp:
                        persisted_trail = json.load(fp)
                except json.JSONDecodeError:
                    pass
            logger().debug(f"Trail from {cached_paths.trail_file} is {persisted_trail}")
            if persisted_trail == trail:
                logger().debug(f"Valid cached branch found for {sub_layer} in {cached_paths.source_directory}")
                return BranchGenerationResult(cached_paths.source_directory,
                                              {cached_paths.source_directory, cached_paths.trail_file})
        logger().debug(f"No valid cached branch found for {sub_layer}")
        return None


static_branch_generator = StaticBranchGenerator()
dynamic_branch_generator = DynamicBranchGenerator()
