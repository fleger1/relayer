# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import annotations

import gettext
import logging
import os
import re
import shlex
import shutil
import sys
from abc import ABC, abstractmethod
from configparser import ConfigParser, RawConfigParser
from contextlib import contextmanager
from dataclasses import dataclass, field
from distutils.util import strtobool
from enum import Enum, auto
from functools import total_ordering
from pathlib import Path
from typing import Union, Sequence, Dict, Iterator, List, Type, Optional, Mapping, Set, Any, MutableSet, Iterable, \
    MutableMapping, AbstractSet

import psutil
from filelock import FileLock
from pkg_resources import iter_entry_points
from sortedcontainers import SortedSet
from xdg import BaseDirectory

from . import _version

__version__ = _version.get_versions()['version']

_logger = None


def logger():
    """Returns the logger instance used in this module."""
    global _logger
    _logger = _logger or logging.getLogger(__name__)
    return _logger


MANUAL_MOUNT_PID = -1
APPLICATION_NAME = "relayer"

gettext.install(APPLICATION_NAME, names={"ngettext"})


# noinspection PyShadowingNames
def get_localized_param_value(section, name, languages, default=None):
    for n in (f"{name}[{lng}]" for lng in languages):
        if n in section:
            return section[n]
    if name in section:
        return section[name]
    return default


def get_param_value(parameter: str, params):
    if parameter not in params:
        raise InvalidLayerException("Missing parameter %s" % parameter)
    return params[parameter]


def read_keys_from_section(key_set: MutableSet[str], config: RawConfigParser, section_name: str) -> MutableSet[str]:
    if section_name in config:
        key_set |= config[section_name].keys()
    return key_set


def cast_or_none(parameter, t) -> Optional[Any]:
    return None if parameter is None else t(parameter)


# Adapted from gettext module
# noinspection PyShadowingNames
def _get_languages():
    languages = []
    for envar in ('LANGUAGE', 'LC_ALL', 'LC_MESSAGES', 'LANG'):
        val = os.environ.get(envar)
        if val:
            languages = val.split(':')
            break
    nelangs = []
    for lang in languages:
        # noinspection PyProtectedMember
        for nelang in gettext._expand_lang(lang):
            if nelang not in nelangs:
                nelangs.append(nelang)
    return nelangs


languages = _get_languages()


def posix_var_name(s: str) -> str:
    s = "".join((c if c.isalnum() or c == '_' else '_' for c in s))
    if len(s) > 0 and s[0].isnumeric():
        s = '_' + s
    return s


@dataclass
class Modifiers:
    disable: Set[str] = field(default_factory=set)
    enable: Set[str] = field(default_factory=set)
    layerset: Optional[str] = None


class Scope(ABC):

    @abstractmethod
    def application_settings_paths(self, application: str) -> Iterable[Path]:
        """Paths to search for global application settings"""
        pass

    @abstractmethod
    def layer_search_paths(self, application: str) -> Iterable[Path]:
        """Paths to search for layers"""
        pass

    @abstractmethod
    def layer_path(self, application: str) -> Path:
        """Path where to save layers"""
        pass

    @abstractmethod
    def cache_path(self, application_settings: ApplicationSettings) -> Path:
        """Root writable cache path for applications (e. g. where dynamic layers are built)"""
        pass

    @abstractmethod
    def cache_search_paths(self, application_settings: ApplicationSettings) -> Iterable[Path]:
        """Root cache search paths for applications (e. g. where expanded dynamic layers are looked up)"""
        pass

    @abstractmethod
    def data_path(self, application: str) -> Path:
        """Path where to save user data (e. g. R/W branch)"""
        pass

    @abstractmethod
    def runtime_path(self, application: str) -> Path:
        """Path where to store temporary runtime data (e. g. mountpoints)"""
        pass

    def lock_file_path(self, application_settings: ApplicationSettings) -> Path:
        return self.runtime_path(
            application_settings.application) / f"{application_settings.layerset.with_layerset(APPLICATION_NAME)}.lock"

    def pid_lock(self, application_settings: ApplicationSettings) -> FileLock:
        logger().debug(f"Acquiring lock for application {application_settings.application}")
        return FileLock(self.lock_file_path(application_settings))

    @staticmethod
    def mkdir_and_return(path: Path) -> Path:
        path.mkdir(parents=True, exist_ok=True)
        return path

    @classmethod
    @abstractmethod
    def describe(cls) -> str:
        pass

    @classmethod
    @abstractmethod
    def priority(cls) -> int:
        pass


class InvalidLayerException(Exception):
    """Exception thrown when a layer is invalid"""


class InvalidLayerSetException(Exception):
    """Exception thrown when a layer set is invalid"""


class UnavailableMountResourceException(Exception):
    """Exception thrown when a resource used for mounting is unavailable"""


@total_ordering
class PureLayer:
    def __init__(self, application_settings: ApplicationSettings, layer_name: str) -> None:
        self.settings = application_settings
        self.layer_name = layer_name

    @staticmethod
    def __is_valid_operand(other):
        return hasattr(other, "settings") and hasattr(other, "layer_name")

    def __repr__(self):
        return f"{self.settings.application}/{self.layer_name}"

    def __eq__(self, other) -> bool:
        if not self.__is_valid_operand(other):
            return NotImplemented
        return (self.settings.application, self.layer_name) == (other.settings.application, other.layer_name)

    def __lt__(self, other) -> bool:
        if not self.__is_valid_operand(other) or self.settings.application != other.settings.application:
            return NotImplemented
        return self.layer_name < other.layer_name

    def __hash__(self) -> int:
        return hash((self.settings.application, self.layer_name))


class CaseSensitiveConfigParser(ConfigParser):

    def optionxform(self, optionstr):
        return optionstr


ENABLED = "enabled"


def str_equals(s1, s2):
    return s1 == s2 or (not s1 and not s2)


class LayerSet:
    CONFIG_FILE = "layerset.conf"

    def __init__(self, application: str, scope: Scope, layerset: Optional[str],
                 _loaded: Optional[MutableSet] = None) -> None:
        self.application = application
        self.scope = scope
        self.layerset = layerset or ''
        self.parent: Optional[LayerSet] = None
        self.description = ''
        self.name = self.layerset
        self.__loaded = _loaded if _loaded is not None else set()
        if self.layerset in self.__loaded:
            raise InvalidLayerSetException(f"Cycle detected when resolving parents for Layer Set {self.layerset}")
        self.__loaded.add(self.layerset)
        self.__load_config()

    def __load_config(self):
        config_file = None
        for layer_search_path in (p / self.with_layerset(ENABLED) for p in
                                  self.scope.layer_search_paths(self.application)):
            f = layer_search_path / self.CONFIG_FILE
            if f.is_file():
                config_file = f
                break
        if config_file is not None:
            # Read config file
            config = CaseSensitiveConfigParser()
            config.read(config_file)
            if "LayerSet" not in config:
                raise InvalidLayerSetException(
                    f"LayerSet configuration file {config_file} is missing a 'LayerSet' section")
            layerset_section = config["LayerSet"]
            # Global properties
            self.name = get_localized_param_value(layerset_section, "Name", languages, self.name)
            self.description = get_localized_param_value(layerset_section, "Description", languages, self.description)
            self.parent = cast_or_none(layerset_section.get("Parent", None), self.__to_parent_layerset)

    def __to_parent_layerset(self, layerset: str) -> LayerSet:
        return LayerSet(self.application, self.scope, layerset, self.__loaded)

    def with_layerset(self, element: str) -> str:
        if not self.layerset:
            return element
        return f"{element}.{self.layerset}"

    def layer_enablement_paths(self) -> Iterable[Path]:
        for p in self.scope.layer_search_paths(self.application):
            yield p / self.with_layerset(ENABLED)
        if self.parent is not None:
            yield from self.parent.layer_enablement_paths()

    def __eq__(self, other):
        if not isinstance(other, LayerSet):
            return NotImplemented
        return self.application == other.application and self.scope == other.scope \
               and str_equals(self.layerset, other.layerset)

    def __hash__(self):
        return hash((self.application, self.scope, (self.layerset or None)))

    def __str__(self):
        return self.layerset


class Layer(PureLayer, ABC):
    LAYER_RE = re.compile("^Layer([1-9][0-9]*)$")

    @classmethod
    def concretize(cls, pure_layer: PureLayer, layerset: LayerSet) -> Optional[Layer]:
        logger().debug(f"Loading layer {pure_layer}")

        # Find actual layer configuration file
        layer_file = None
        for layer_search_path in layerset.scope.layer_search_paths(pure_layer.settings.application):
            f = layer_search_path / (pure_layer.layer_name + ".layer")
            if f.is_file():
                layer_file = f
                break

        if layer_file is None:
            return None

        # Check if layer is enabled
        symlink = None
        for layer_enablement_path in layerset.layer_enablement_paths():
            f = layer_enablement_path / (pure_layer.layer_name + ".layer")
            if f.is_symlink():
                symlink = f
                break

        return cls(pure_layer.settings, pure_layer.layer_name, layer_file, symlink)

    def __init__(self,
                 application_settings: ApplicationSettings,
                 layer_name: str,
                 layer_file: Path,
                 symlink: Optional[Path]) -> None:
        super().__init__(application_settings, layer_name)
        self.layer_file = layer_file
        self.symlink = symlink

        # Read layer file
        self.config = CaseSensitiveConfigParser(allow_no_value=True)
        self.config.read(self.layer_file)

        if "Layers" not in self.config:
            raise InvalidLayerException(f"Layer {self.layer_file} is missing a 'Layers' section")
        layers_section = self.config["Layers"]
        self.enrich_for_interpolation(layers_section)

        # Global properties
        self.version = get_param_value("Version", layers_section)
        self.provides = read_keys_from_section({self.layer_name}, self.config, "Provides")
        self.conflicts = read_keys_from_section(set(), self.config, "Conflicts")
        self.depends = read_keys_from_section(set(), self.config, "Depends")
        self.if_exists = cast_or_none(layers_section.get("IfExists", None), Path)
        self.name = get_localized_param_value(layers_section, "Name", languages, self.layer_name)
        self.description = get_localized_param_value(layers_section, "Description", languages, "")

        # Sub Layers
        sub_layer_numbers = [int(match.group(1)) for match in map(self.LAYER_RE.match, layers_section) if match]
        sub_layer_numbers.sort()
        self.sub_layers = [SubLayer(self, layers_section[f"Layer{i}"]) for i in sub_layer_numbers]

    @classmethod
    def enrich_for_interpolation(cls, section: MutableMapping[str, str]) -> None:
        cls.setdefaults(section, {
            "UID": str(os.getuid()),
            "HOME": str(Path.home()),
            "XDG_DATA_HOME": BaseDirectory.xdg_data_home,
            "XDG_CONFIG_HOME": BaseDirectory.xdg_config_home,
            "XDG_CACHE_HOME": BaseDirectory.xdg_cache_home
        })

    @classmethod
    def setdefaults(cls, section: MutableMapping[str, str], properties: Mapping[str, str]) -> None:
        for p, v in properties.items():
            section.setdefault(p, v)

    @property
    def layers_section(self) -> MutableMapping[str, str]:
        return self.config["Layers"]

    def __str__(self):
        return self.layer_name


class SubLayer:
    def __init__(self, layer: Layer, sublayer_name: str) -> None:
        self.layer = layer
        self.sublayer_name = sublayer_name
        self.layer.enrich_for_interpolation(self.layer_section)
        self.type = get_param_value("Type", self.layer_section)
        self.if_exists = cast_or_none(self.layer_section.get("IfExists", None), Path)
        self.name = get_localized_param_value(self.layer_section, "Name", languages, self.sublayer_name)
        self.description = get_localized_param_value(self.layer_section, "Description", languages, "")
        self.provides = read_keys_from_section({self.layer_name}, self.layer.config,
                                               self.prefixed_section_name("Provides"))
        self.conflicts = read_keys_from_section(set(), self.layer.config, self.prefixed_section_name("Conflicts"))
        self.depends = read_keys_from_section(set(), self.layer.config, self.prefixed_section_name("Depends"))

    def prefixed_section_name(self, section_name: str) -> str:
        return f"{self.sublayer_name}:{section_name}"

    @property
    def layer_name(self) -> str:
        return f"{self.layer}/{self.sublayer_name}"

    @property
    def layer_section(self) -> MutableMapping[str, str]:
        return self.layer.config[self.prefixed_section_name("Layer")]

    def __str__(self):
        return self.layer_name


class BranchGenerator(ABC):
    @abstractmethod
    def generate_branch(self, mount_environment: MountEnvironment, sub_layer: SubLayer) -> BranchGenerationResult:
        pass


@dataclass
class BranchesResult:
    branches: List[Path] = field(default_factory=list)
    cached_paths: Set[Path] = field(default_factory=set)

    def add(self, layer_result: BranchGenerationResult):
        self.branches.append(layer_result.path)
        self.cached_paths |= layer_result.cached_paths


@dataclass
class BranchGenerationResult:
    path: Path
    cached_paths: Set[Path] = field(default_factory=set)


@dataclass
class LayerCheck:
    layer: Layer
    symlink_enabled: bool
    missing_depends: AbstractSet[str]
    conflicting_layers: AbstractSet[str]
    exists: bool
    sub_layer_checker: Iterator[SubLayerCheck] = field(init=False)
    force_enabled: bool = False
    force_disabled: bool = False

    @property
    def enabled(self) -> bool:
        return not self.force_disabled \
               and (self.symlink_enabled or self.force_enabled) \
               and len(self.missing_depends) == 0 \
               and len(self.conflicting_layers) == 0 \
               and self.exists


@dataclass
class SubLayerCheck:
    layer_check: LayerCheck
    sub_layer: SubLayer
    missing_depends: AbstractSet[str]
    conflicting_layers: AbstractSet[str]
    exists: bool

    @property
    def enabled(self) -> bool:
        return self.layer_check.enabled \
               and len(self.missing_depends) == 0 \
               and len(self.conflicting_layers) == 0 \
               and self.exists


def layer_checker(layers: Iterator[Layer], modifiers: Modifiers = Modifiers()) -> Iterator[LayerCheck]:
    provided = set()

    def sub_layer_checker(check: LayerCheck) -> Iterator[SubLayerCheck]:
        nonlocal provided
        for sub_layer in check.layer.sub_layers:
            sub_check = SubLayerCheck(check, sub_layer,
                                      sub_layer.depends - provided,
                                      provided.intersection(sub_layer.conflicts),
                                      sub_layer.if_exists is None or sub_layer.if_exists.exists())
            if sub_check.enabled:
                provided |= sub_layer.provides
            yield sub_check

    for layer in layers:
        layer_check = LayerCheck(layer, layer.symlink is not None and layer.symlink.is_file(),
                                 layer.depends - provided, provided.intersection(layer.conflicts),
                                 layer.if_exists is None or layer.if_exists.exists(),
                                 force_disabled=not modifiers.disable.isdisjoint(layer.provides),
                                 force_enabled=not modifiers.enable.isdisjoint(layer.provides))
        if layer_check.enabled:
            provided |= layer.provides
        layer_check.sub_layer_checker = sub_layer_checker(layer_check)
        yield layer_check
        # layer_check.sub_layer_checker needs to be exhausted
        # in order to make sure the provided set is being properly populated
        # Only required if the layer is enabled
        if layer_check.enabled:
            for _ in layer_check.sub_layer_checker:
                pass


@dataclass
class MountEnvironment:
    mount_driver: MountDriver
    previous_sub_layers: List[SubLayer] = field(default_factory=list)


class MountDriverException(Exception):
    pass


class MountDriver(ABC):

    def __init__(self) -> None:
        pass

    def make_lower_branches(self, layer_list: Iterator[Layer],
                            up_to: Optional[str] = None, modifiers: Modifiers = Modifiers()) -> BranchesResult:
        lower_branches = BranchesResult()
        mount_environment = MountEnvironment(self)

        for l_check in layer_checker(layer_list, modifiers):
            if not l_check.symlink_enabled and l_check.layer.symlink:
                logger().debug(f"{l_check.layer} skipped because layer has been explicitly disabled by "
                               f"{l_check.layer.symlink}")
            if len(l_check.missing_depends) > 0:
                logger().debug(f"{l_check.layer} skipped due to missing dependencies: {l_check.missing_depends}")
            if len(l_check.conflicting_layers) > 0:
                logger().debug(f"{l_check.layer} skipped due to conflicting layers: {l_check.conflicting_layers}")
            if not l_check.exists:
                logger().debug(f"{l_check.layer} skipped because {l_check.layer.if_exists} does not exist")
            for sl_check in l_check.sub_layer_checker:
                if up_to == sl_check.sub_layer.layer_name:
                    break
                if l_check.enabled:
                    if len(sl_check.missing_depends) > 0:
                        logger().debug(
                            f"{sl_check.sub_layer} skipped due to missing dependencies: {sl_check.missing_depends}")
                    if len(sl_check.conflicting_layers) > 0:
                        logger().debug(
                            f"{sl_check.sub_layer} skipped due to conflicting layers: {sl_check.conflicting_layers}")
                    if not sl_check.exists:
                        logger().debug(
                            f"{sl_check.sub_layer} skipped because {sl_check.sub_layer.if_exists} does not exist")
                    if sl_check.enabled:
                        branch_generator = all_branch_generators[sl_check.sub_layer.type]
                        branch = branch_generator.generate_branch(mount_environment, sl_check.sub_layer)
                        lower_branches.add(branch)
                        mount_environment.previous_sub_layers.append(sl_check.sub_layer)
            else:
                continue
            break

        return lower_branches

    @staticmethod
    def make_runtime_dir(application_settings: ApplicationSettings, suffix: str) -> Path:
        """Create and return a temporary directory"""
        return application_settings.scope.mkdir_and_return(
            application_settings.scope.runtime_path(application_settings.application) / suffix)

    @classmethod
    def make_final_mount_point(cls, application_settings: ApplicationSettings) -> Path:
        """Create and return the final mountpoint directory"""
        return cls.make_runtime_dir(application_settings, application_settings.layerset.with_layerset("mountpoint"))

    @staticmethod
    def make_rw_user_dir(application_settings: ApplicationSettings) -> Path:
        """Create and return the R/W user directory"""
        return application_settings.scope.mkdir_and_return(
            application_settings.scope.data_path(
                application_settings.application) / application_settings.layerset.with_layerset("rw"))

    @abstractmethod
    def mount(self,
              application_settings: ApplicationSettings,
              upper_dir: Optional[Path] = None,
              up_to: Optional[str] = None) -> Path:
        pass

    @abstractmethod
    def umount(self, application_settings: ApplicationSettings, lazy: bool = False) -> None:
        pass

    @classmethod
    @abstractmethod
    def describe(cls) -> str:
        pass

    @classmethod
    @abstractmethod
    def priority(cls) -> int:
        pass


class PIDManager:

    def __init__(self, application_settings: ApplicationSettings) -> None:
        self.application_settings = application_settings

    def __make_pids_path(self, application: str) -> Path:
        scope = self.application_settings.scope
        layerset = self.application_settings.layerset
        return scope.mkdir_and_return(scope.runtime_path(application) / layerset.with_layerset("pids"))

    def running_pids(self, application: str) -> List[int]:
        pids_path: Path = self.__make_pids_path(application)
        running_pids: List[int] = []
        for i in pids_path.glob("*.pid"):
            if i.is_file():
                try:
                    pid = int(str(i.name)[:-4])
                except ValueError:
                    pass
                else:
                    if pid == MANUAL_MOUNT_PID or psutil.pid_exists(pid):
                        running_pids.append(pid)
                    else:
                        # Stale PID file
                        logger().debug(f"Removing stale pid file {i}")
                        i.unlink()
        logger().debug(f"Running pids are {running_pids}")
        return running_pids

    def register_pid(self, application: str, pid: int) -> None:
        pid_file: Path = self.__make_pids_path(application) / (str(pid) + ".pid")
        with pid_file.open("w") as f:
            logger().info(f"Registering pid {pid}")
            f.write(str(pid))

    def unregister_pid(self, application: str, pid: int) -> None:
        pid_file: Path = self.__make_pids_path(application) / (str(pid) + ".pid")
        if pid_file.is_file():
            logger().info(f"Unregistering pid {pid}")
            pid_file.unlink()


class ApplicationSettings:

    def __init__(self, application: str, scope: Scope, modifiers: Modifiers = Modifiers()) -> None:
        self.application = application
        self.scope = scope
        self.modifiers = modifiers
        self.refresh()

    # noinspection PyAttributeOutsideInit
    def refresh(self) -> None:
        self.__layer_list: Optional[List[Layer]] = None
        self.__settings_loaded = False
        self.__use_copy_on_write = True
        self.__name = self.application
        self.__description = ""
        self.__layerset = None

    def __load_settings(self) -> None:
        if self.__settings_loaded:
            return
        config_parser = ConfigParser()
        files_read = config_parser.read(self.scope.application_settings_paths(self.application))
        logger().debug(f"Read settings from {files_read}")
        if "RelayerSettings" in config_parser:
            logger().debug("RelayerSettings section found")
            settings = config_parser["RelayerSettings"]
            if "UseCopyOnWrite" in settings:
                self.__use_copy_on_write = bool(strtobool(settings["UseCopyOnWrite"]))
                logger().debug(f"Set copy on write to {self.__use_copy_on_write}")
            self.__name = get_localized_param_value(settings, "Name", languages, "")
            self.__description = get_localized_param_value(settings, "Description", languages, "")
            self.__desktop_entry = cast_or_none(settings.get("DesktopEntry", None), Path)
            self.__command = cast_or_none(settings.get("Command", None), shlex.split)
        self.__settings_loaded = True

    def make_child_environment(self, mount_point: Path) -> Mapping[str, str]:
        child_env = dict(os.environ)
        child_env[posix_var_name(f"RELAYER_MOUNTPOINT_{self.application}")] = str(mount_point.absolute())
        return child_env

    @property
    def use_copy_on_write(self) -> bool:
        self.__load_settings()
        return self.__use_copy_on_write

    @property
    def name(self) -> str:
        self.__load_settings()
        return self.__name

    @property
    def description(self) -> str:
        self.__load_settings()
        return self.__description

    @property
    def desktop_entry(self) -> Optional[Path]:
        self.__load_settings()
        return self.__desktop_entry

    @property
    def command(self) -> Optional[List[str]]:
        self.__load_settings()
        return self.__command

    @property
    def layerset(self) -> LayerSet:
        if self.__layerset is None:
            self.__layerset = LayerSet(self.application, self.scope, self.modifiers.layerset)
        return self.__layerset

    @property
    def layer_list(self) -> List[Layer]:
        if self.__layer_list is None:
            # noinspection PyAttributeOutsideInit
            self.__layer_list = self.concretize(self.__get_layers())
        return self.__layer_list

    def concretize(self, layers: Iterator[PureLayer]) -> List[Layer]:
        return [Layer.concretize(layer, self.layerset) for layer in layers if layer]

    def __get_layers(self) -> SortedSet[PureLayer]:
        # noinspection PyArgumentList
        layer_set = SortedSet()
        for layer_search_path in self.scope.layer_search_paths(self.application):
            if layer_search_path.exists():
                layer_set.update(
                    PureLayer(self, p.stem) for p in layer_search_path.glob("*.layer") if
                    p.is_file())
        return layer_set

    def __str__(self):
        return self.name


class RelayerController:

    def __init__(self, application: str,
                 scope: Optional[Scope] = None,
                 mount_driver: Optional[MountDriver] = None) -> None:
        if scope is None:
            from relayer.scopes import UserScope
            scope = UserScope()
        if mount_driver is None:
            mount_driver = next(iter(all_mount_drivers.values()))()
        self.modifiers = Modifiers()
        self.application_settings = ApplicationSettings(application, scope, self.modifiers)
        self.pid_manager = PIDManager(self.application_settings)
        self.mount_driver = mount_driver

    def __mount(self, pid: int) -> Path:
        with self.application_settings.scope.pid_lock(self.application_settings):
            if len(self.pid_manager.running_pids(self.application_settings.application)) == 0:
                logger().info(f"Mount application {self.application_settings.application} as pid {pid}")
                self.mount_driver.umount(self.application_settings)
                self.mount_driver.mount(self.application_settings)
                self.pid_manager.register_pid(self.application_settings.application, pid)
        return self.mount_driver.make_final_mount_point(self.application_settings)

    def __umount(self, pid: int, lazy: bool = False) -> Path:
        with self.application_settings.scope.pid_lock(self.application_settings):
            self.pid_manager.unregister_pid(self.application_settings.application, pid)
            if len(self.pid_manager.running_pids(self.application_settings.application)) == 0:
                logger().info(f"Unmount application {self.application_settings.application} as pid {pid}")
                self.mount_driver.umount(self.application_settings, lazy=lazy)
        return self.mount_driver.make_final_mount_point(self.application_settings)

    def mount_path(self) -> Path:
        return self.mount_driver.make_final_mount_point(self.application_settings)

    def layersets(self) -> Set[LayerSet]:
        layerset_ids = {None}
        enabled_prefix = ENABLED + '.'
        for lsp in self.application_settings.scope.layer_search_paths(self.application_settings.application):
            if not lsp.is_dir():
                continue
            for ls in lsp.glob(f'{enabled_prefix}*'):
                if ls.is_dir() and ls.name != enabled_prefix:
                    layerset_ids.add(ls.name[len(enabled_prefix):])
        return {LayerSet(self.application_settings.application, self.application_settings.scope, layerset)
                for layerset in layerset_ids}

    def build(self) -> None:
        logger().info(f"Build {self.application_settings.application} caches")
        with self.application_settings.scope.pid_lock(self.application_settings):
            if len(self.pid_manager.running_pids(self.application_settings.application)) == 0:
                results = self.mount_driver.make_lower_branches(self.application_settings.layer_list,
                                                                modifiers=self.modifiers)

                # Clean-up cache
                cache_path = self.application_settings.scope.cache_path(self.application_settings).resolve()
                valid = {p for p in (r.resolve() for r in results.cached_paths) if cache_path in p.parents}
                max_depth = max((len(p.relative_to(cache_path).parts)) for p in valid) if len(valid) > 0 else 1
                for depth in range(max_depth):
                    glob_exp = "/".join(["*"] * (depth + 1))
                    for child in cache_path.glob(glob_exp):
                        for p in valid:
                            if child == p or child in p.parents:
                                break
                        else:
                            if child.is_dir() and not child.is_symlink():
                                logger().info(f"Removing obsolete cache directory {child}")
                                shutil.rmtree(child)
                            else:
                                logger().info(f"Removing obsolete cache file {child}")
                                child.unlink()

    def mount(self) -> Path:
        logger().info(f"Manually mount {self.application_settings.application}")
        return self.__mount(MANUAL_MOUNT_PID)

    def umount(self, lazy: bool = False) -> Path:
        logger().info(f"Manually unmount {self.application_settings.application}")
        return self.__umount(MANUAL_MOUNT_PID, lazy=lazy)

    @contextmanager
    def auto_mount(self, lazy: bool = False):
        path = self.__mount(os.getpid())
        try:
            yield path
        finally:
            self.__umount(os.getpid(), lazy=lazy)

    def check_layers(self) -> Iterator[LayerCheck]:
        return layer_checker(self.application_settings.layer_list, self.modifiers)

    def run(self, command: Union[bytes, str, Sequence[Union[bytes, str, os.PathLike]]],
            change_directory: bool = False, shell: bool = False, lazy: bool = False) -> int:
        logger().info(f"Run command {command} for application {self.application_settings.application}")
        with self.auto_mount(lazy=lazy) as mount_path:
            from subprocess import CompletedProcess, run
            process: CompletedProcess = run(command, check=False, stdout=sys.stdout, stderr=sys.stderr,
                                            cwd=mount_path if change_directory else None, shell=shell,
                                            env=self.application_settings.make_child_environment(mount_path))
            logger().debug(f"Command return code is {process.returncode}")
            return process.returncode

    def enable(self, layer_name: str) -> SwitchReturnStatus:
        for check in self.check_layers():
            if check.layer.layer_name == layer_name:
                if check.layer.symlink and check.symlink_enabled:
                    return SwitchReturnStatus.NO_OP
                enabled_path = self.application_settings.scope.layer_path(
                    self.application_settings.application) / self.application_settings.layerset.with_layerset(ENABLED)
                if check.layer.symlink and check.layer.symlink.parent == enabled_path:
                    logger().debug(f"Deleting {check.layer.symlink}")
                    check.layer.symlink.unlink()
                    self.application_settings.refresh()
                    r = self.enable(layer_name)
                    return SwitchReturnStatus.OK if r == SwitchReturnStatus.NO_OP else r
                symlink = (enabled_path / f"{check.layer.layer_name}.layer")
                logger().debug(f"Creating {symlink} -> {check.layer.layer_file}")
                self.application_settings.scope.mkdir_and_return(enabled_path)
                symlink.symlink_to(check.layer.layer_file)
                self.application_settings.refresh()
                return SwitchReturnStatus.OK
        return SwitchReturnStatus.NO_MATCHING_LAYER

    def disable(self, layer_name: str) -> SwitchReturnStatus:
        for check in self.check_layers():
            if check.layer.layer_name == layer_name:
                # No SymLink or /dev/null SymLink
                if not check.layer.symlink or (check.layer.symlink and not check.symlink_enabled):
                    return SwitchReturnStatus.NO_OP
                enabled_path = self.application_settings.scope.layer_path(
                    self.application_settings.application) / self.application_settings.layerset.with_layerset(ENABLED)
                if check.layer.symlink and check.layer.symlink.parent == enabled_path:
                    logger().debug(f"Deleting {check.layer.symlink}")
                    check.layer.symlink.unlink()
                    self.application_settings.refresh()
                    r = self.disable(layer_name)
                    return SwitchReturnStatus.OK if r == SwitchReturnStatus.NO_OP else r
                symlink = (enabled_path / f"{check.layer.layer_name}.layer")
                logger().debug(f"Creating {symlink} -> /dev/null")
                self.application_settings.scope.mkdir_and_return(enabled_path)
                symlink.symlink_to("/dev/null")
                self.application_settings.refresh()
                return SwitchReturnStatus.OK
        return SwitchReturnStatus.NO_MATCHING_LAYER


class SwitchReturnStatus(Enum):
    OK = auto()
    NO_OP = auto()
    NO_MATCHING_LAYER = auto()


all_scopes: Dict[str, Type[Scope]] = {
    entry_point.name: entry_point.load()
    for entry_point in sorted(iter_entry_points("relayer.scopes"),
                              key=lambda entry_point: entry_point.load().priority(),
                              reverse=True)
}

all_mount_drivers: Dict[str, Type[MountDriver]] = {
    entry_point.name: entry_point.load()
    for entry_point in sorted(iter_entry_points("relayer.mount_drivers"),
                              key=lambda entry_point: entry_point.load().priority(),
                              reverse=True)
}

all_branch_generators: Dict[str, BranchGenerator] = {
    entry_point.name: entry_point.load()
    for entry_point in iter_entry_points("relayer.branch_generators")
}
