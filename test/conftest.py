# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
import pytest
import re
import shlex
from pathlib import Path

from relayer import ApplicationSettings, Modifiers
from test import LocalScope, TEST_APP

MAXVAL = 30


@pytest.fixture
def scope(request, tmpdir_factory) -> LocalScope:
    name = request.node.name
    name = re.sub(r"[\W]", "_", name)
    if len(name) > MAXVAL:
        name = name[:MAXVAL]
    d = tmpdir_factory.mktemp(name, numbered=True)
    scope = LocalScope(Path(d))

    # Prepare branches

    # Static branch
    my_static_branch = scope.mkdir_and_return(scope.tmpdir / "branches" / "my_static")
    my_staticd_branch = scope.mkdir_and_return(scope.tmpdir / "branches" / "my_staticd")
    with (my_static_branch / "staticfile").open("w") as staticfile:
        staticfile.write("Static Branch Test File")
    with (my_staticd_branch / "staticfile2").open("w") as staticfile2:
        staticfile2.write("Static Branch Test File 2")
    with (scope.mkdir_and_return(scope.layer_search_paths("")[0]) / "1-static.layer").open("w") as f:
        f.write("""[Layers]
        Name=Test static layer
        Description=A static layer used for testing located in layer_search[0]
        Version=1.0
        Layer1=Static
        
        [Static:Layer]
        Type=static
        SourceDirectory=%s
        """ % my_static_branch.absolute())
    with (scope.mkdir_and_return(scope.layer_search_paths("")[1]) / "1-static.layer").open("w") as f:
        f.write("""[Layers]
        Name=Test static layer
        Description=A static layer used for testing located in layer_search[1]
        Version=1.0
        Layer1=Static
        
        [Static:Layer]
        Type=static
        SourceDirectory=%s
        """ % "/fakebranch")
    (scope.mkdir_and_return(scope.layer_search_paths("")[1] / "enabled") / "1-static.layer").symlink_to(
        "../1-static.layer")
    (scope.mkdir_and_return(scope.layer_search_paths("")[1] / "enabled.alt") / "1-static.layer").symlink_to(
        "../1-static.layer")

    with (scope.mkdir_and_return(scope.layer_search_paths("")[1]) / "5-static-disabled.layer").open("w") as f:
        f.write("""[Layers]
        Name=Disabled test static layer
        Description=A static layer that will be disabled -- except in the "alt" layer set
        Version=1.0
        Layer1=Static
        
        [Static:Layer]
        Type=static
        SourceDirectory=%s
        """ % my_staticd_branch.absolute())
    (scope.mkdir_and_return(scope.layer_search_paths("")[1] / "enabled") / "5-static-disabled.layer").symlink_to(
        "../5-static-disabled.layer")
    (scope.mkdir_and_return(scope.layer_search_paths("")[1] / "enabled.alt") / "5-static-disabled.layer").symlink_to(
        "../1-static.layer")

    # Dynamic branch
    my_dynamic_branch_generator = scope.tmpdir / "my_dynamic_generator"
    with my_dynamic_branch_generator.open("w") as f:
        f.write("""#! /bin/env bash
        
        echo "Generated Test File" > "$1/dynamicfile"
        """)
    os.chmod(my_dynamic_branch_generator, 0o700)
    with (scope.mkdir_and_return(scope.layer_search_paths("")[1]) / "2-dynamic.layer").open("w") as f:
        f.write("""[Layers]
        Name=Test dynamic layer
        Description=A dynamic layer used for testing located in layer_search[1]
        Version=1.0
        Layer1=Dynamic
        
        [Dynamic:Layer]
        Type=dynamic
        SourceGenerator=%s @mount_point@
        """ % shlex.quote(str(my_dynamic_branch_generator.absolute())))
    (scope.mkdir_and_return(scope.layer_search_paths("")[1] / "enabled") / "2-dynamic.layer").symlink_to(
        "../2-dynamic.layer")
    (scope.mkdir_and_return(scope.layer_search_paths("")[1] / "enabled.alt") / "2-dynamic.layer").symlink_to(
        "../2-dynamic.layer")

    # For testing expansion and IfExists
    with (scope.mkdir_and_return(scope.layer_search_paths("")[1]) / "3-static-user.layer").open("w") as f:
        f.write("""[Layers]
        Name=User static layer
        Description=Another static layer used for testing that won't satisfy the IfExists condition
        Version=%(UID)s
        Layer1=Static
        
        [Static:Layer]
        Type=static
        SourceDirectory=%(XDG_DATA_HOME)s/pytest/relayer
        IfExists="/non_existingehjzihféy"_ài"
        """)
    (scope.mkdir_and_return(scope.layer_search_paths("")[1] / "enabled") / "3-static-user.layer").symlink_to(
        "../3-static-user.layer")

    # Null branch
    (scope.mkdir_and_return(scope.layer_search_paths("")[1] / "enabled") / "4-disabled.layer") \
        .symlink_to("/dev/null")
    (scope.mkdir_and_return(scope.layer_search_paths("")[0] / "enabled") / "5-static-disabled.layer") \
        .symlink_to("/dev/null")

    return scope


@pytest.fixture
def alt_modifiers() -> Modifiers:
    return Modifiers(layerset='alt')


@pytest.fixture
def application_settings(scope) -> ApplicationSettings:
    return ApplicationSettings(TEST_APP, scope)


@pytest.fixture
def alt_application_settings(scope, alt_modifiers) -> ApplicationSettings:
    return ApplicationSettings(TEST_APP, scope, alt_modifiers)
