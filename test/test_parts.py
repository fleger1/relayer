# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os

from relayer import PureLayer, Layer


def test_get_layers(application_settings) -> None:
    # When
    layers = application_settings.layer_list

    # Then
    assert len(layers) == 4
    assert layers[0].layer_name == "1-static"
    assert layers[0].settings == application_settings
    assert layers[1].layer_name == "2-dynamic"
    assert layers[1].settings == application_settings
    assert layers[2].layer_name == "3-static-user"
    assert layers[2].settings == application_settings
    assert layers[3].layer_name == "5-static-disabled"
    assert layers[3].settings == application_settings


def test_concretize_layer(application_settings) -> None:
    # Given
    static_purelayer = PureLayer(application_settings, "1-static")
    dynamic_purelayer = PureLayer(application_settings, "2-dynamic")
    static_user_purelayer = PureLayer(application_settings, "3-static-user")
    null_purelayer = PureLayer(application_settings, "4-disabled")
    static_disabled_purelayer = PureLayer(application_settings, "5-static-disabled")

    # When
    static_layer = Layer.concretize(static_purelayer, application_settings.layerset)
    dynamic_layer = Layer.concretize(dynamic_purelayer, application_settings.layerset)
    static_user_layer = Layer.concretize(static_user_purelayer, application_settings.layerset)
    null_layer = Layer.concretize(null_purelayer, application_settings.layerset)
    static_disabled_layer = Layer.concretize(static_disabled_purelayer, application_settings.layerset)

    # Then
    assert static_layer.name == "Test static layer"
    assert static_layer.description == "A static layer used for testing located in layer_search[0]"
    assert static_layer.layer_name == "1-static"
    assert static_layer.settings == application_settings
    assert static_layer.version == "1.0"
    assert len(static_layer.sub_layers) == 1
    assert static_layer.sub_layers[0].sublayer_name == "Static"
    assert static_layer.sub_layers[0].layer_name == "1-static/Static"
    assert static_layer.sub_layers[0].type == "static"

    assert dynamic_layer.name == "Test dynamic layer"
    assert dynamic_layer.description == "A dynamic layer used for testing located in layer_search[1]"
    assert dynamic_layer.layer_name == "2-dynamic"
    assert dynamic_layer.settings == application_settings
    assert dynamic_layer.version == "1.0"
    assert len(dynamic_layer.sub_layers) == 1
    assert dynamic_layer.sub_layers[0].sublayer_name == "Dynamic"
    assert dynamic_layer.sub_layers[0].layer_name == "2-dynamic/Dynamic"
    assert dynamic_layer.sub_layers[0].type == "dynamic"

    assert static_user_layer.name == "User static layer"
    assert static_user_layer.description == "Another static layer used for testing " \
                                            "that won't satisfy the IfExists condition"
    assert static_user_layer.layer_name == "3-static-user"
    assert static_user_layer.settings == application_settings
    assert static_user_layer.version == str(os.getuid())
    assert len(static_user_layer.sub_layers) == 1
    assert static_user_layer.sub_layers[0].sublayer_name == "Static"
    assert static_user_layer.sub_layers[0].layer_name == "3-static-user/Static"
    assert static_user_layer.sub_layers[0].type == "static"

    assert null_layer is None

    assert static_disabled_layer.name == "Disabled test static layer"
    assert static_disabled_layer.description == 'A static layer that will be disabled -- except in the "alt" layer set'
    assert static_disabled_layer.layer_name == "5-static-disabled"
    assert static_disabled_layer.settings == application_settings
    assert static_disabled_layer.version == "1.0"
    assert len(static_disabled_layer.sub_layers) == 1
    assert static_disabled_layer.sub_layers[0].sublayer_name == "Static"
    assert static_disabled_layer.sub_layers[0].layer_name == "5-static-disabled/Static"
    assert static_disabled_layer.sub_layers[0].type == "static"
