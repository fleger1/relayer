# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from pathlib import Path
from typing import Sequence, Iterator

from relayer import Scope, ApplicationSettings

TEST_APP = "myapp"


class LocalScope(Scope):

    def layer_path(self, application: str) -> Path:
        return self.tmpdir / "etc"

    @classmethod
    def describe(cls) -> str:
        return "Test scope"

    @classmethod
    def priority(cls) -> int:
        return -1

    def __init__(self, tmpdir: Path) -> None:
        self.tmpdir = tmpdir

    def layer_search_paths(self, application: str) -> Sequence[Path]:
        return [self.tmpdir / "etc",
                self.tmpdir / "etc2",
                self.tmpdir / "etc3"]

    def application_settings_paths(self, application: str) -> Iterator[Path]:
        yield self.tmpdir / (application + ".conf")

    def cache_path(self, application_settings: ApplicationSettings) -> Path:
        return self.mkdir_and_return(self.tmpdir / "cache" / application_settings.layerset.with_layerset('layers'))

    def data_path(self, application: str) -> Path:
        return self.mkdir_and_return(self.tmpdir / "share")

    def runtime_path(self, application: str) -> Path:
        return self.mkdir_and_return(self.tmpdir / "tmp")

    def cache_search_paths(self, application_settings: ApplicationSettings) -> Iterator[Path]:
        yield self.cache_path(application_settings)
