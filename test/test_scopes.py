# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
from pathlib import Path

from relayer import ApplicationSettings
from xdg import BaseDirectory

from relayer.scopes import SystemScope, UserScope


def test_system_scope(application_settings: ApplicationSettings):
    scope = SystemScope()
    assert scope.cache_path(application_settings) == Path("/var/cache/relayer/myapp/layers")
    cache_paths = list(scope.cache_search_paths(application_settings))
    assert len(cache_paths) == 1
    assert cache_paths[0] == Path("/var/cache/relayer/myapp/layers")


def test_user_scope(application_settings: ApplicationSettings):
    scope = UserScope()
    assert scope.cache_path(application_settings) == Path(BaseDirectory.save_cache_path("relayer", "myapp", "layers"))
    cache_paths = list(scope.cache_search_paths(application_settings))
    assert len(cache_paths) == 2
    assert cache_paths[0] == Path(BaseDirectory.save_cache_path("relayer", "myapp", "layers"))
    assert cache_paths[1] == Path("/var/cache/relayer/myapp/layers")


def test_system_scope_with_layerset(alt_application_settings: ApplicationSettings):
    scope = SystemScope()
    assert scope.cache_path(alt_application_settings) == Path("/var/cache/relayer/myapp/layers.alt")
    cache_paths = list(scope.cache_search_paths(alt_application_settings))
    assert len(cache_paths) == 1
    assert cache_paths[0] == Path("/var/cache/relayer/myapp/layers.alt")


def test_user_scope_with_layerset(alt_application_settings: ApplicationSettings):
    scope = UserScope()
    assert scope.cache_path(alt_application_settings) == Path(BaseDirectory.save_cache_path("relayer", "myapp",
                                                                                            "layers.alt"))
    cache_paths = list(scope.cache_search_paths(alt_application_settings))
    assert len(cache_paths) == 2
    assert cache_paths[0] == Path(BaseDirectory.save_cache_path("relayer", "myapp", "layers.alt"))
    assert cache_paths[1] == Path("/var/cache/relayer/myapp/layers.alt")
