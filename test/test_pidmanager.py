# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
from pathlib import Path

from relayer import PIDManager, MANUAL_MOUNT_PID, ApplicationSettings


def test_register_pid(application_settings: ApplicationSettings) -> None:
    # Given
    pid_manager = PIDManager(application_settings)
    pid = os.getpid()

    # When
    pid_manager.register_pid(application_settings.application, pid)

    # Then
    check_pid_file_exists(application_settings, pid)


def test_register_pid_with_layerset(alt_application_settings: ApplicationSettings) -> None:
    # Given
    pid_manager = PIDManager(alt_application_settings)
    pid = os.getpid()

    # When
    pid_manager.register_pid(alt_application_settings.application, pid)

    # Then
    check_pid_file_exists(alt_application_settings, pid, 'pids.alt')


def get_pid_path(settings: ApplicationSettings, pid: int, pidsdir: str = 'pids') -> Path:
    return settings.scope.runtime_path(settings.application) / pidsdir / (str(pid) + ".pid")


def check_pid_file_exists(settings: ApplicationSettings, pid: int, pidsdir: str = 'pids') -> None:
    pid_path = get_pid_path(settings, pid, pidsdir)
    assert pid_path.is_file()
    with pid_path.open() as file_content:
        assert file_content.readline() == str(pid)


def check_pid_file_does_not_exist(settings: ApplicationSettings, pid: int, pidsdir: str = 'pids') -> None:
    pid_path = get_pid_path(settings, pid, pidsdir)
    assert not pid_path.exists()


def test_running_pids(application_settings: ApplicationSettings) -> None:
    # Given
    pid_manager = PIDManager(application_settings)
    pid = os.getpid()
    fake_pid = -42
    # PID guaranteed to exist (current process)
    pid_manager.register_pid(application_settings.application, pid)
    # Special PID value for manual mounts
    pid_manager.register_pid(application_settings.application, MANUAL_MOUNT_PID)
    # PID guaranteed not to exist
    pid_manager.register_pid(application_settings.application, fake_pid)

    # When
    running_pids = pid_manager.running_pids(application_settings.application)

    # Then
    assert len(running_pids) == 2
    assert pid in running_pids
    assert MANUAL_MOUNT_PID in running_pids
    check_pid_file_exists(application_settings, pid)
    check_pid_file_exists(application_settings, MANUAL_MOUNT_PID)
    check_pid_file_does_not_exist(application_settings, fake_pid)


def test_unregister_pid(application_settings: ApplicationSettings) -> None:
    # Given
    pid_manager = PIDManager(application_settings)
    pid = os.getpid()
    pid_manager.register_pid(application_settings.application, pid)

    # When
    pid_manager.unregister_pid(application_settings.application, pid)

    # Then
    check_pid_file_does_not_exist(application_settings, pid)
