# Copyright (c) 2018-2024 Florian Léger
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import stat

from relayer import ApplicationSettings
from relayer.mount_drivers import UnionFSFuseMountDriver


def test_mount_cycle(application_settings: ApplicationSettings) -> None:
    mount_driver = UnionFSFuseMountDriver()
    try:
        mount_path = mount_driver.mount(application_settings)
        assert mount_path.is_dir()
        assert mount_path.is_mount()
        assert (mount_path / "staticfile").is_file()
        assert not (mount_path / "staticfile2").exists()
        assert (mount_path / "dynamicfile").is_file()

        with (mount_path / "newfile").open("w") as new_file:
            new_file.write("new file")
    finally:
        mount_driver.umount(application_settings)

    assert not mount_path.is_mount()

    assert not (mount_path / "staticfile").exists()
    assert not (mount_path / "staticfile2").exists()
    assert not (mount_path / "dynamicfile").exists()
    assert not (mount_path / "newfile").exists()

    try:
        mount_path = mount_driver.mount(application_settings)
        assert mount_path.is_dir()
        assert mount_path.is_mount()
        assert (mount_path / "staticfile").is_file()
        assert not (mount_path / "staticfile2").exists()
        assert (mount_path / "dynamicfile").is_file()
        assert (mount_path / "newfile").is_file()
    finally:
        mount_driver.umount(application_settings)

    assert not mount_path.is_mount()


def test_mount_cycle_with_layerset(alt_application_settings: ApplicationSettings) -> None:
    application_settings = alt_application_settings
    mount_driver = UnionFSFuseMountDriver()
    try:
        mount_path = mount_driver.mount(application_settings)
        assert mount_path.is_dir()
        assert mount_path.is_mount()
        assert (mount_path / "staticfile").is_file()
        assert (mount_path / "staticfile2").is_file()
        assert (mount_path / "dynamicfile").is_file()

        with (mount_path / "newfile").open("w") as new_file:
            new_file.write("new file")
    finally:
        mount_driver.umount(application_settings)

    assert not mount_path.is_mount()

    assert not (mount_path / "staticfile").exists()
    assert not (mount_path / "staticfile2").exists()
    assert not (mount_path / "dynamicfile").exists()
    assert not (mount_path / "newfile").exists()

    try:
        mount_path = mount_driver.mount(application_settings)
        assert mount_path.is_dir()
        assert mount_path.is_mount()
        assert (mount_path / "staticfile").is_file()
        assert (mount_path / "staticfile2").is_file()
        assert (mount_path / "dynamicfile").is_file()
        assert (mount_path / "newfile").is_file()
    finally:
        mount_driver.umount(application_settings)

    assert not mount_path.is_mount()


def test_lazy_unmount(application_settings: ApplicationSettings) -> None:
    mount_driver = UnionFSFuseMountDriver()
    mount_path = mount_driver.mount(application_settings)
    assert mount_path.is_dir()
    assert mount_path.is_mount()
    assert (mount_path / "staticfile").is_file()
    assert (mount_path / "dynamicfile").is_file()

    with (mount_path / "newfile").open("w") as new_file:
        mount_driver.umount(application_settings, lazy=True)
        new_file.write("new file")

    assert not mount_path.is_mount()


def test_metadata_permissions_fixup(application_settings: ApplicationSettings) -> None:
    mount_driver = UnionFSFuseMountDriver()
    try:
        mount_path = mount_driver.mount(application_settings)
        staticfile = mount_path / "staticfile"
        staticfile.unlink()
    finally:
        mount_driver.umount(application_settings)

    upper_dir = mount_driver.make_rw_user_dir(application_settings)
    metadata_dir = upper_dir / ".unionfs"
    assert metadata_dir.is_dir()
    mode = metadata_dir.stat().st_mode
    assert mode & stat.S_IRGRP
    assert mode & stat.S_IXGRP
    assert mode & stat.S_IROTH
    assert mode & stat.S_IXOTH
    metadata_writeout = metadata_dir / "staticfile_HIDDEN~"
    mode = metadata_writeout.stat().st_mode
    assert mode & stat.S_IRGRP
    assert mode & stat.S_IROTH
